package main

import (
	"fmt"
	"gitlab.com/pgmtc/le/pkg/agent"
	"gitlab.com/pgmtc/le/pkg/builder"
	"gitlab.com/pgmtc/le/pkg/common"
	"gitlab.com/pgmtc/le/pkg/config"
	"gitlab.com/pgmtc/le/pkg/local"
	"os"
	"path/filepath"
	"reflect"
	"strings"
)

var (
	modules = map[string]common.Module{
		"config":  config.Module{},
		"local":   local.Module{},
		"builder": builder.Module{},
		"agent":   agent.Module{},
	}
	cnf       common.ConfigProvider
	globalCnf common.ConfigProvider
	logger    = common.ConsoleLogger{}
)

func init() {
	globalCnf = common.NewFileSystemConfig("~/.le")
	// test if there is .le directory with config present
	if _, err := os.Stat(filepath.Join(".le", "config.yaml")); os.IsNotExist(err) {
		cnf = common.NewFileSystemConfig("~/.le")
	} else {
		cnf = common.NewFileSystemConfig(".le")
	}

}

func main() {
	args := os.Args[1:]
	globalConfigErr := globalCnf.LoadConfig()
	localConfigErr := cnf.LoadConfig()
	if len(args) > 1 && args[0] == "builder" {
		// ignore missing config for builder
	} else if !(len(args) >= 2 && args[0] == "config" && args[1] == "init") {
		if localConfigErr != nil && globalConfigErr != nil {
			logger.Errorf("%s\n", localConfigErr.Error())
			logger.Errorf("%s\n", globalConfigErr.Error())
			logger.Errorf("Neither local or global config exists. Try initializing config by running '%s config init' for local, or '%s config init --global' for global config\n", os.Args[0], os.Args[0])
			os.Exit(1)
		}
	}

	if len(args) == 0 {
		printHelp()
		os.Exit(1)
	}
	moduleName := args[0]
	if _, ok := modules[moduleName]; !ok {
		logger.Errorf("Module %s does not exist\n", moduleName)
		os.Exit(1)
	}

	moduleArgs := args[1:]
	os.Exit(runModule(modules[moduleName], moduleArgs...))
}

func runModule(module common.Module, args ...string) int {
	actions := module.GetActions()
	actionName := "default"
	var actionArgs []string

	if len(args) > 0 {
		actionName = args[0]
		actionArgs = args[1:]
	}

	if _, ok := actions[actionName]; !ok {
		availableActions := reflect.ValueOf(actions).MapKeys()
		logger.Errorf("Missing action '%s'. Available actions: %s\n", actionName, availableActions)
		return 1
	}

	//logger.Infof("Current profile: %s\n", cnf.Config().Profile)
	action := actions[actionName]
	//start := time.Now()
	if err := action.Run(common.ModuleContext{Log: logger, Config: cnf, GlobalConfig: globalCnf}, actionArgs...); err != nil {
		logger.Errorf("Action Error: %s\n", strings.Replace(err.Error(), "\n", "", -1))
		return 2
	}
	//elapsed := time.Since(start)
	//logger.Debugf("Action took %s\n", elapsed)
	return 0
}

func printHelp(messages ...string) {
	availableModules := reflect.ValueOf(modules).MapKeys()

	fmt.Printf("Current profile: ")
	fmt.Printf("%s\n", cnf.Config().Profile)
	for _, message := range messages {
		fmt.Printf(message)
	}

	fmt.Printf("Please provide module, available modules: ")
	fmt.Printf("%s\n", availableModules)
	fmt.Printf(" syntax : %s [module] [action]\n", os.Args[0])
	fmt.Printf(" example: %s local status\n", os.Args[0])
}
