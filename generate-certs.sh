cn=$1
if [ "$cn" == "" ]
then
  cn="server"
fi

openssl genrsa -out ~/.le/server.key 2048
openssl req -new -x509 -sha256 -key ~/.le/server.key -out ~/.le/${cn}.crt -days 3650 -subj "/C=GB/ST=Scotland/L=Edinburgh/O=Test/CN=${cn}"
openssl req -new -sha256 -key ~/.le/server.key -out ~/.le/${cn}.csr -subj "/C=GB/ST=Scotland/L=Edinburgh/O=Test/CN=${cn}"
openssl x509 -req -sha256 -in ~/.le/${cn}.csr -signkey ~/.le/server.key -out ~/.le/${cn}.crt -days 3650
cp ~/.le/${cn}.crt ~/.le/server.crt
echo "=================== INFO ====================="
echo "server.key and server.crt are used by the server"
echo "${cn}.crt is used by the client to access ${cn}"
echo "=============================================="