package docker

import (
	"fmt"
	"gitlab.com/pgmtc/le/pkg/common"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
)

// Builder is an interface for docker builder
type Builder struct{}

// BuildImage builds a docker image
func (Builder) BuildImage(ctx common.ModuleContext, image string, buildRoot string, dockerFile string, buildArgs []string, noCache bool) error {
	return buildImage(ctx, image, buildRoot, dockerFile, buildArgs, noCache)
}

// BuildFile builds a file
func (Builder) BuildFile(ctx common.ModuleContext, file string, buildRoot string, builderImage string, builderCmd string, noCache bool) error {
	dockerFile, err := createBuildFileDockerFile(ctx, builderImage, builderCmd, file)
	if err != nil {
		return err
	}
	defer os.Remove(dockerFile)
	image := "le-file-build-image"
	if err = buildImage(ctx, image, buildRoot, dockerFile, []string{}, noCache); err != nil {
		return fmt.Errorf("error when creating builder image: %s", err.Error())
	}

	// Create container
	hostPort := 9999
	cmp := common.Component{
		Name:          "le-file-build",
		DockerID:      "le-file-build",
		Image:         "le-file-build-image",
		ContainerPort: 8043, HostPort: hostPort,
	}
	_ = removeComponent(cmp, ctx.Log.Infof)
	if err = createContainer(cmp, ctx.Log.Infof); err != nil {
		return fmt.Errorf("error when creating builder image: %s", err.Error())
	}
	defer removeComponent(cmp, ctx.Log.Infof)

	// Start container
	if err = startComponent(cmp, ctx.Log.Infof); err != nil {
		return fmt.Errorf("error when creating builder image: %s", err.Error())
	}
	// Wait until container responds and download the file
	targetFile := filepath.Join(ctx.WorkingDir, file)
	downloadURL := fmt.Sprintf("http://localhost:%d/%s", hostPort, file)
	ctx.Log.Infof("Downloading file %s from the builder image to %s\n", downloadURL, file)
	if err := downloadFile(downloadURL, targetFile); err != nil {
		return fmt.Errorf("error when downloading result file: %s", err.Error())
	}
	ctx.Log.Infof("File %s has been downloaded and saved to %s\n", file, targetFile)
	return nil
}

// ExportCmd exports docker build command with parameters used by le
func (Builder) ExportCmd(ctx common.ModuleContext, image string, buildRoot string, dockerFile string, buildArgs []string, file string, builderImage string, builderCmd string) error {
	if image == "" {
		return fmt.Errorf("export-cmd is not implemented for file type")
	}
	ctx.Log.Infof("%s\n", BuilderCommand(image, buildRoot, dockerFile, buildArgs))
	return nil
}

func createBuildFileDockerFile(ctx common.ModuleContext, builderImage string, builderCmd string, file string) (resultFilePath string, resultErr error) {
	// Create docker file
	tempDir, err := ioutil.TempDir("", "")
	dockerFile, err := os.Create(path.Join(tempDir, "Dockerfile"))

	if err != nil {
		resultErr = fmt.Errorf("error when creating temporary dockerfile: %s", err.Error())
		return
	}

	fileRows := []string{
		fmt.Sprintf("FROM %s as builder", builderImage),
		fmt.Sprintf("WORKDIR /build"),
		fmt.Sprintf("COPY . ."),
		fmt.Sprintf("RUN %s", builderCmd),
		fmt.Sprintf("RUN ls -ltr"),
		fmt.Sprintf("FROM pierrezemb/gostatic"),
		fmt.Sprintf("COPY --from=builder /build/%s /srv/http/", file),
	}

	fileContents := ""
	for i, row := range fileRows {
		if ctx.Log != nil {
			ctx.Log.Debugf("Dockerfile %d: %s\n", i+1, row)
		}
		fileContents += row + "\n"
	}

	if _, resultErr = dockerFile.WriteString(fileContents); resultErr != nil {
		return
	}
	resultFilePath = dockerFile.Name()
	return
}
