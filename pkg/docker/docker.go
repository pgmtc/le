package docker

import (
	"encoding/json"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/stdcopy"
	"github.com/docker/go-connections/nat"
	"github.com/mholt/archiver"
	"github.com/olekukonko/tablewriter"
	"github.com/pkg/errors"
	"gitlab.com/pgmtc/le/pkg/common"
	"golang.org/x/net/context"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func getClient() *client.Client {
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		panic(err)
	}
	cli.NegotiateAPIVersion(ctx)
	return cli
}

func getImages() (images []string) {
	out, _ := getClient().ImageList(context.Background(), types.ImageListOptions{})
	for _, img := range out {
		for _, tag := range img.RepoTags {
			images = append(images, tag)
		}
	}
	return
}

func printLogs(component common.Component, follow bool) error {
	if container, err := getContainer(component); err == nil {
		options := types.ContainerLogsOptions{ShowStdout: true, ShowStderr: true, Follow: follow, Timestamps: false}
		out, err := getClient().ContainerLogs(context.Background(), container.ID, options)
		if err != nil {
			return err
		}
		stdcopy.StdCopy(os.Stdout, os.Stderr, out)
		return nil
	}
	return errors.Errorf("Error when getting container logs for '%s' (%s)\n", component.Name, component.DockerID)
}

func getContainers() map[string]types.Container {
	containers, _ := getClient().ContainerList(context.Background(), types.ContainerListOptions{
		All: true,
	})

	containerMap := make(map[string]types.Container)
	for _, cont := range containers {
		for _, cName := range cont.Names {
			containerName := cName[1:len(cName)]
			containerMap[containerName] = cont
		}
	}
	return containerMap
}

func startComponent(component common.Component, logger func(format string, a ...interface{})) error {
	if container, err := getContainer(component); err == nil {
		logger("Starting container '%s' for component '%s'\n", component.DockerID, component.Name)

		if err := getClient().ContainerStart(context.Background(), container.ID, types.ContainerStartOptions{}); err != nil {
			return err
		}
		return nil
	}
	return errors.Errorf("Starting container '%s' for component '%s': Not found. Create it first\n", component.Name, component.DockerID)
}

func stopContainer(component common.Component, logger func(format string, a ...interface{})) error {
	if container, err := getContainer(component); err == nil {
		logger("Stopping container '%s' for component '%s'\n", component.DockerID, component.Name)
		if err := getClient().ContainerStop(context.Background(), container.ID, nil); err != nil {
			return err
		}
		return nil
	}
	return errors.Errorf("Stopping container '%s' for component '%s': Not found found. Nothing to stop\n", component.Name, component.DockerID)
}

func removeComponent(component common.Component, logger func(format string, a ...interface{})) error {
	if container, err := getContainer(component); err == nil {
		if container.State == "running" {
			if err := stopContainer(component, logger); err != nil {
				return err
			}
		}
		logger("Removing container '%s' for component '%s'\n", component.DockerID, component.Name)
		if err := getClient().ContainerRemove(context.Background(), container.ID, types.ContainerRemoveOptions{}); err != nil {
			return err
		}
		return nil
	}
	return errors.Errorf("Removing container '%s' for component '%s': Not found. Nothing to remove\n", component.Name, component.DockerID)
}

func createContainer(component common.Component, logger func(format string, a ...interface{})) error {
	if component.Name == "" || component.DockerID == "" || component.Image == "" {
		return errors.New("Missing container Name, DockerID or Image")
	}

	if _, err := getContainer(component); err == nil {
		return errors.Errorf("Component %s already exist (%s). If you want to recreate, then please stop and remove it first", component.Name, component.DockerID)
	}
	logger("Creating container '%s' for component '%s'\n", component.DockerID, component.Name)
	var exposedPorts nat.PortSet
	var portMap nat.PortMap

	// Backwards compatibility to support HostPort and ContainerPort
	if component.ContainerPort > 0 && component.HostPort > 0 {
		//exposedPorts = nat.PortSet{nat.Port(exposePort): struct{}{}}
		//portMap = map[nat.Port][]nat.PortBinding{nat.Port(exposePort): {{HostIP: "0.0.0.0", HostPort: mapPort}}}
		logger(" - port %d will be mapped to host port %d\n", component.ContainerPort, component.HostPort)
		component.Ports = append(component.Ports, fmt.Sprintf("%d:%d", component.HostPort, component.ContainerPort))
	}

	if len(component.Ports) > 0 {
		logger(" - mapping component ports: %v\n", component.Ports)
		portMap = map[nat.Port][]nat.PortBinding{}
		exposedPorts = map[nat.Port]struct{}{}
		for _, cPort := range component.Ports {
			hostPort, containerPort, err := parsePort(cPort)
			if err != nil {
				return err
			}
			exposedPorts[nat.Port(containerPort)] = struct{}{}
			portMap[nat.Port(containerPort)] = []nat.PortBinding{{HostIP: "0.0.0.0", HostPort: hostPort}}
		}
	}

	// Mount AWS login credentials
	mounts, err := parseMounts(component)
	if err != nil {
		return errors.Errorf("error when parsing mounts: %s", err.Error())
	}

	// Fill in env variables to env variables
	envVariables := parseEnvVariables(component.Env)

	statusMessages := []string{}
	if len(envVariables) > 0 {
		statusMessages = append(statusMessages, fmt.Sprintf(" - env variables %v added", envVariables))
	}
	if len(mounts) > 0 {
		statusMessages = append(statusMessages, fmt.Sprintf(" - mounts %v added", component.Mounts))
	}
	if len(component.ExtraHosts) > 0 {
		statusMessages = append(statusMessages, fmt.Sprintf(" - extra hosts %v added", component.ExtraHosts))
	}
	if len(component.Links) > 0 {
		statusMessages = append(statusMessages, fmt.Sprintf(" - links %v added", component.Links))
	}
	logger(strings.Join(statusMessages, "\n"))

	_, err = getClient().ContainerCreate(context.Background(), &container.Config{
		Image:        component.Image,
		Env:          envVariables,
		ExposedPorts: exposedPorts,
	}, &container.HostConfig{
		ExtraHosts:   component.ExtraHosts,
		PortBindings: portMap,
		Links:        component.Links,
		Mounts:       mounts,
	}, nil, component.DockerID)
	if err != nil {
		return err
	}

	logger("\n")
	return nil
}

func getContainer(component common.Component) (types.Container, error) {
	var nilCont types.Container
	dockerID := component.DockerID
	containerMap := getContainers()
	if cont, ok := containerMap[dockerID]; ok {
		return cont, nil
	}
	return nilCont, errors.New("Can't find the container")
}

func removeImage(component common.Component, logger func(format string, a ...interface{})) error {
	logger("removing Image for '%s' (%s) ... \n", component.Name, component.Image)
	name := "docker"
	args := []string{"rmi", component.Image}
	cmd := exec.Command(name, args...)
	if err := cmd.Run(); err != nil {
		return errors.Errorf("Error when removing the image: %s", err.Error())
	}
	return nil
}

func pullImage(component common.Component, logger func(format string, a ...interface{})) error {
	var pullOptions types.ImagePullOptions
	authString, err := getAuthString(component.Auth)
	if err != nil {
		return errors.Errorf("error when obtaining authentication details: %s", err.Error())
	}
	if authString != "" {
		pullOptions = types.ImagePullOptions{
			RegistryAuth: authString,
		}
	}
	dockerImage := getFullImagePath(component.Image)
	out, err := getClient().ImagePull(context.Background(), dockerImage, pullOptions)
	if err != nil {
		return err
	}
	defer out.Close()

	d := json.NewDecoder(out)

	type Event struct {
		Stream         string `json:"stream"`
		Status         string `json:"status"`
		Error          string `json:"error"`
		Progress       string `json:"progress"`
		ProgressDetail struct {
			Current int `json:"current"`
			Total   int `json:"total"`
		} `json:"progressDetail"`
	}

	var event *Event
	for {
		if err := d.Decode(&event); err != nil {
			if err == io.EOF {
				break
			}
		}
		switch true {
		case event.Error != "":
			return errors.Errorf("\nbuild error: %s", event.Error)
		case event.Progress != "" || event.Status != "":
			logger("\r%s: %s", event.Status, event.Progress)
			if event.ProgressDetail.Current == 0 {
				logger("\n")
			}
		case strings.TrimSuffix(event.Stream, "\n") != "":
			logger("%s", event.Stream)
		}

	}

	logger("\n")
	return nil
}

func printStatus(allComponents []common.Component, verbose bool, follow bool, writer io.Writer) error {

	containerMap := getContainers()
	images := getImages()
	table := tablewriter.NewWriter(writer)
	table.SetHeader([]string{"Component", "Image (built or pulled)", "Container Exists (created)", "State", "HostPort", "HTTP"})

	for _, cmp := range allComponents {
		exists := "NO"
		imageExists := "NO"
		state := "missing"
		responding := ""
		if container, ok := containerMap[cmp.DockerID]; ok {
			exists = "YES"
			state = container.State
			if state == "running" {
				responding, _ = isResponding(cmp)
			}
		}

		if common.ArrContains(images, cmp.Image) || common.ArrContains(images, strings.Replace(cmp.Image, "library/", "", 1)) {
			imageExists = "YES"
		}

		// Some formatting
		var imageString = cmp.Image
		if !verbose {
			imageSplit := strings.Split(imageString, "/")
			imageString = imageSplit[len(imageSplit)-1]
		}
		switch imageExists {
		case "YES":
			imageExists = fmt.Sprintf("✓ %s", imageString)
		case "NO":
			imageExists = fmt.Sprintf("✓ %s", imageString)
		}

		switch exists {
		case "YES":
			exists = fmt.Sprintf("✓ %s", cmp.DockerID)
		case "NO":
			exists = fmt.Sprintf("  %s", cmp.DockerID)
		}

		switch state {
		case "running":
			state = fmt.Sprintf("✓ %s", state)
		case "missing":
			state = fmt.Sprintf("  %s", state)
		default:
			state = fmt.Sprintf("● %s", state)
		}

		switch responding {
		case "200":
			responding = fmt.Sprintf("✓  %s", responding)
		default:
			responding = fmt.Sprintf("   %s", responding)
		}

		var ports []string
		if cmp.HostPort > 0 {
			ports = append(ports, fmt.Sprintf("%s:%s", strconv.Itoa(cmp.HostPort), strconv.Itoa(cmp.ContainerPort)))
		}
		for _, port := range cmp.Ports {
			ports = append(ports, port)
		}

		table.Append([]string{cmp.Name, imageExists, exists, state, strings.Join(ports, ","), responding})

	}

	if follow {
		writer.Write([]byte("\033[H\033[2J")) // Clear screen
	}
	writer.Write([]byte("\r"))
	table.Render()
	return nil
}

func parseMounts(cmp common.Component) (mounts []mount.Mount, resultErr error) {
	if len(cmp.Mounts) > 0 {
		for _, mnt := range cmp.Mounts {
			mountSpec := strings.Split(mnt, ":")
			if len(mountSpec) == 2 {
				mntSrc := common.ParsePath(mountSpec[0])
				mntDst := common.ParsePath(mountSpec[1])
				if _, err := os.Stat(mntSrc); os.IsNotExist(err) {
					resultErr = errors.Errorf("error when adding mount: source directory %s does not exist", mntSrc)
					return
				}
				if _, err := os.Stat(mntSrc); !os.IsNotExist(err) {
					mounts = append(mounts, mount.Mount{Type: mount.TypeBind, Source: mntSrc, Target: mntDst})
				}
			} else {
				resultErr = errors.Errorf("Unable to parse mount item: %s", mnt)
			}
		}
	}
	return
}

func buildImage(ctx common.ModuleContext, image string, buildRoot string, dockerFile string, buildArgs []string, noCache bool) (resultErr error) {
	defer func() {
		if e := recover(); e != nil {
			resultErr = fmt.Errorf("%v", e)
		}
	}()
	log := ctx.Log
	if dockerFile == "" || image == "" || buildRoot == "" {
		return errors.Errorf("Missing parameters: image: %s, buildRoot: %s, dockerFile: %s", image, buildRoot, dockerFile)
	}
	log.Debugf("Building image %s'\n - Build Root: %s\n - Dockerfile: %s\n - No Cache: %t\n", image, buildRoot, dockerFile, noCache)

	log.Debugf("Creating context tar ... \n")
	contextTarFileName, returnError := mkContextTar(buildRoot, dockerFile)
	if returnError != nil {
		return returnError
	}
	defer os.Remove(contextTarFileName)
	log.Debugf("ModuleContext tar: %s\n", contextTarFileName)

	log.Debugf("Building docker context from %s\n", contextTarFileName)
	dockerBuildContext, returnError := os.Open(contextTarFileName)
	if returnError != nil {
		return returnError
	}
	defer dockerBuildContext.Close()

	cli := getClient()
	args := parseBuildArgs(buildArgs)

	options := types.ImageBuildOptions{
		SuppressOutput: false,
		Remove:         true,
		ForceRemove:    true,
		PullParent:     false,
		Tags:           []string{image},
		Dockerfile:     "Dockerfile",
		BuildArgs:      args,
		NoCache:        noCache,
	}

	log.Debugf("Starting docker build ...\n")
	buildResponse, err := cli.ImageBuild(context.Background(), dockerBuildContext, options)
	if err != nil {
		log.Errorf("%s\n", err.Error())
		return
	}
	log.Debugf("Finished with build\n")
	d := json.NewDecoder(buildResponse.Body)

	type Event struct {
		Stream         string `json:"stream"`
		Status         string `json:"status"`
		Error          string `json:"error"`
		Progress       string `json:"progress"`
		ProgressDetail struct {
			Current int `json:"current"`
			Total   int `json:"total"`
		} `json:"progressDetail"`
	}

	prevProgressLength := 0
	lastWasProgress := false
	var event *Event
	for {
		if err := d.Decode(&event); err != nil {
			if err == io.EOF {
				break
			}
			panic(err)
		}

		//log.Debugf("%+v\n", event)
		switch true {
		case event.Error != "":
			if lastWasProgress {
				log.Debugf("\n")
			}
			return errors.Errorf("\nbuild error: %s", event.Error)
		case strings.TrimSuffix(event.Stream, "\n") != "":
			if lastWasProgress {
				log.Debugf("\n")
			}
			trimmed := strings.TrimSuffix(event.Stream, string([]byte{10, 27, 91, 48, 109}))
			trimmed = strings.TrimRight(trimmed, "\n")
			log.Debugf("[docker]: %s\n", trimmed)
			lastWasProgress = false
		case event.Progress != "" || event.Status != "":
			progressMsg := fmt.Sprintf("[docker]: %s: %s", event.Status, event.Progress)
			if event.Status == "Downloading" {
				log.Debugf("\r%"+fmt.Sprintf("-%ds", prevProgressLength), progressMsg)
			}
			if event.ProgressDetail.Current == 0 {
				log.Debugf("%s\n", progressMsg)
			}
			prevProgressLength = len(progressMsg)
			lastWasProgress = true
		}
		event.Stream = ""
	}
	return nil
}

func mkContextTar(contextDir string, dockerFile string) (tarFile string, resultErr error) {
	tmpDir, resultErr := ioutil.TempDir("", "")
	if resultErr != nil {
		return
	}
	tarFile = tmpDir + "/docker-context.tar"
	resultErr = archiver.Archive([]string{contextDir + "/", dockerFile}, tarFile)
	return
}

func parseBuildArgs(buildArgs []string) (result map[string]*string) {
	result = map[string]*string{}
	for _, buildArg := range buildArgs {
		var argName, argValue string
		argSplit := strings.Split(buildArg, ":")
		switch len(argSplit) {
		case 1:
			argName = argSplit[0]
			argValue = argSplit[0]
		default:
			argName = argSplit[0]
			argValue = argSplit[1]
		}
		argName = strings.Trim(argName, " ")
		argValue = strings.Trim(argValue, " ")
		argValue = os.ExpandEnv(argValue)
		if argName != "" {
			result[argName] = &argValue
		}
	}
	return
}

func parseEnvVariable(envVar string) (result string) {
	// Split into tuples
	var argName, argValue string
	argSplit := strings.Split(envVar, "=")
	if len(argSplit) != 2 {
		// pass back - let others deal with it
		return envVar
	}
	argName = argSplit[0]
	argValue = os.ExpandEnv(argSplit[1])
	return fmt.Sprintf("%s=%s", argName, argValue)
}

func parseEnvVariables(envVariables []string) (result []string) {
	for _, envVar := range envVariables {
		result = append(result, parseEnvVariable(envVar))
	}
	return
}

// canonical name
func getFullImagePath(componentImage string) string {
	dockerImageSplit := strings.Split(componentImage, ":") // split to image and tag
	dockerImageName := dockerImageSplit[0]
	if !strings.Contains(dockerImageName, ".") {
		// add docker.io
		return fmt.Sprintf("docker.io/%s", componentImage)
	}
	return componentImage
}

func parsePort(in string) (hostPort, containerPort string, resultErr error) {
	parseError := fmt.Errorf("error when parsing port %s, expecing 'inport:outport' or 'port' format", in)
	inSplit := strings.Split(in, ":")
	if len(inSplit) > 2 || len(inSplit) < 1 {
		resultErr = parseError
		return
	}
	inSplit[0] = strings.Trim(inSplit[0], " ")
	if inSplit[0] == "" {
		resultErr = parseError
		return
	}
	if len(inSplit) == 1 {
		hostPort, containerPort = inSplit[0], inSplit[0]
		return
	}
	inSplit[1] = strings.Trim(inSplit[1], " ")
	if inSplit[0] == "" || inSplit[1] == "" {
		resultErr = parseError
		return
	}
	hostPort = inSplit[0]
	containerPort = inSplit[1]
	return
}
