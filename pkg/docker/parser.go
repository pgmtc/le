package docker

import (
	"fmt"
	"gitlab.com/pgmtc/le/pkg/common"
	"strings"
)

// RunnerCommand returns docker command equivalent used by le
func RunnerCommand(cmp common.Component) (result string) {
	result = ""
	var paramArr []string
	paramArr = append(paramArr, "docker run -d")
	paramArr = append(paramArr, fmt.Sprintf("--name %s", cmp.DockerID))
	// old ports - for compatibility
	if cmp.HostPort != 0 || cmp.ContainerPort != 0 {
		cmp.Ports = append(cmp.Ports, fmt.Sprintf("%d:%d", cmp.HostPort, cmp.ContainerPort))
	}
	// Ports
	for _, item := range cmp.Ports {
		paramArr = append(paramArr, fmt.Sprintf("--publish %s", item))
	}
	// Environment variables
	for _, item := range cmp.Env {
		paramArr = append(paramArr, fmt.Sprintf("--env %s", item))
	}
	// Links
	for _, item := range cmp.Links {
		paramArr = append(paramArr, fmt.Sprintf("--link %s", item))
	}
	// Volumes
	for _, item := range cmp.Mounts {
		paramArr = append(paramArr, fmt.Sprintf("--volume %s", item))
	}
	// Extra Hosts
	for _, item := range cmp.ExtraHosts {
		paramArr = append(paramArr, fmt.Sprintf("--add-host %s", item))
	}

	paramArr = append(paramArr, fmt.Sprintf("%s", cmp.Image))
	result = strings.Join(paramArr, " ")
	return
}

// BuilderCommand returns docker build command equivalent used by le
func BuilderCommand(image string, buildRoot string, dockerFile string, buildArgs []string) (result string) {
	var paramArr []string
	paramArr = append(paramArr, "docker build")
	paramArr = append(paramArr, fmt.Sprintf("-f %s", dockerFile))
	for _, buildArg := range buildArgs {
		paramArr = append(paramArr, fmt.Sprintf("--build-arg %s", buildArg))
	}
	paramArr = append(paramArr, fmt.Sprintf("--tag %s", image))
	paramArr = append(paramArr, fmt.Sprintf("%s", buildRoot))
	return strings.Join(paramArr, " ")
}
