package docker

import (
	"gitlab.com/pgmtc/le/pkg/common"
	"io/ioutil"
	"testing"
)

func TestBuilder_BuildImage(t *testing.T) {
	// WIP:
	builder := Builder{}
	err := builder.BuildImage(common.ModuleContext{}, "", "", "", []string{}, false)
	if err == nil {
		t.Errorf("Expected error, got nothing")
	}
}

func TestBuilder_BuildFile(t *testing.T) {
	// WIP:
	builder := Builder{}
	err := builder.BuildFile(common.ModuleContext{}, "", "", "", "", false)
	if err == nil {
		t.Errorf("Expected error, got nothing")
	}
}

func TestBuilder_ExportCmd(t *testing.T) {
	// WIP:
	builder := Builder{}
	// func (Builder) ExportCmd(ctx common.ModuleContext, image string, buildRoot string, dockerFile string, buildArgs []string, file string, builderImage string, builderCmd string) error {
	err := builder.ExportCmd(common.ModuleContext{Log: setUpForRun()}, "image", "", "dockerfile", []string{"buildarg1", "buildarg2"}, "", "", "")
	if err != nil {
		t.Errorf("Unexpected error: %s", err.Error())
	}
	err = builder.ExportCmd(common.ModuleContext{Log: setUpForRun()}, "", "buildroot", "", []string{"buildarg1", "buildarg2"}, "file", "builderImage", "builderCmd")
	if err == nil {
		t.Errorf("Expected error, got nothing")
	}
}

func Test_createBuildFileDockerFile(t *testing.T) {
	file, err := createBuildFileDockerFile(common.ModuleContext{Log: common.ConsoleLogger{}}, "file", "builder-image", "builder-cmd")
	if err != nil {
		t.Errorf("Unexpected error: %s", err.Error())
	}
	bytes, err := ioutil.ReadFile(file)
	expected := "FROM file as builder\n" +
		"WORKDIR /build\n" +
		"COPY . .\n" +
		"RUN builder-image\n" +
		"RUN ls -ltr\n" +
		"FROM pierrezemb/gostatic\n" +
		"COPY --from=builder /build/builder-cmd /srv/http/\n"
	if string(bytes) != expected {
		t.Errorf("\nExpected ------------ :\n%s\nGot ----------------- :\n%s", expected, string(bytes))
	}
}
