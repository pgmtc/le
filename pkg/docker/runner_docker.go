package docker

import (
	"fmt"
	"gitlab.com/pgmtc/le/pkg/common"
	"strconv"
	"time"
)

// Runner is a runner for docker
type Runner struct {
}

// ExportCmd returns docker command which can be run elsewhere to reflect what's done by le
func (Runner) ExportCmd(ctx common.ModuleContext, cmp common.Component) error {
	ctx.Log.Infof("%s\n", RunnerCommand(cmp))
	return nil
}

// Status returns status
func (Runner) Status(ctx common.ModuleContext, args ...string) error {
	config := ctx.Config
	var verbose bool
	var follow bool
	var followLength int

	if len(args) > 0 && args[0] == "-v" || len(args) > 1 && args[1] == "-v" {
		verbose = true
	}

	// This could be improved - generalized
	if len(args) > 0 && args[0] == "-f" || len(args) > 1 && args[1] == "-f" {
		follow = true
		switch true {
		case len(args) > 1 && args[0] == "-f":
			i, err := strconv.Atoi(args[1])
			if err == nil {
				followLength = i
			}
		case len(args) > 2 && args[1] == "-f":
			i, err := strconv.Atoi(args[2])
			if err == nil {
				followLength = i
			}
		}
		follow = true
	}

	if !follow {
		return printStatus(config.CurrentProfile().Components, verbose, follow, ctx.Log)
	}
	counter := 0
	for {
		printStatus(config.CurrentProfile().Components, verbose, follow, ctx.Log)
		fmt.Println("local status: ", time.Now().Format("2006-01-02 15:04:05"))
		counter++
		time.Sleep(1 * time.Second)
		if counter == followLength {
			break
		}
	}

	return nil
}

// Create creates container
func (Runner) Create(ctx common.ModuleContext, cmp common.Component) error {
	return createContainer(cmp, ctx.Log.Infof)
}

// Remove removes container
func (Runner) Remove(ctx common.ModuleContext, cmp common.Component) error {
	return removeComponent(cmp, ctx.Log.Infof)
}

// Start starts container
func (Runner) Start(ctx common.ModuleContext, cmp common.Component) error {
	return startComponent(cmp, ctx.Log.Infof)
}

// Stop stops container
func (Runner) Stop(ctx common.ModuleContext, cmp common.Component) error {
	return stopContainer(cmp, ctx.Log.Infof)
}

// Pull pulls container
func (Runner) Pull(ctx common.ModuleContext, cmp common.Component) error {
	return pullImage(cmp, ctx.Log.Infof)
}

// Logs returns logs of the container
func (Runner) Logs(ctx common.ModuleContext, cmp common.Component, follow bool) error {
	return printLogs(cmp, follow)
}
