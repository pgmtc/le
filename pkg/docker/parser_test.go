package docker

import (
	"gitlab.com/pgmtc/le/pkg/common"
	"testing"
)

func TestDockerCommand(t *testing.T) {
	type args struct {
		cmp common.Component
	}
	tests := []struct {
		name       string
		args       args
		wantResult string
	}{
		{
			name: "test",
			args: args{cmp: common.Component{
				Name:          "my-component",
				DockerID:      "my-component-docker-id",
				Image:         "my-component-image",
				ContainerPort: 8080,
				HostPort:      80,
				Ports:         []string{"90:9090", "99:9099"},
				Env:           []string{"ENV1=var1", "ENV2=var2"},
				Links:         []string{"LNK1:lnk1", "LNK2:lnk2"},
				Mounts:        []string{"/host-dir:/container-dir", "/host-dir-2:/container-dir-2"},
				ExtraHosts:    []string{"host1:1.1.1.1", "host2:2.2.2.2"},
			}},
			wantResult: "docker run -d " +
				"--name my-component-docker-id " +
				"--publish 90:9090 --publish 99:9099 --publish 80:8080 " +
				"--env ENV1=var1 --env ENV2=var2 " +
				"--link LNK1:lnk1 --link LNK2:lnk2 " +
				"--volume /host-dir:/container-dir --volume /host-dir-2:/container-dir-2 " +
				"--add-host host1:1.1.1.1 --add-host host2:2.2.2.2 " +
				"my-component-image",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := RunnerCommand(tt.args.cmp); gotResult != tt.wantResult {
				t.Errorf("RunnerCommand():\n  %v\nwant\n  %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestBuilderCommand(t *testing.T) {
	type args struct {
		image      string
		buildRoot  string
		dockerFile string
		buildArgs  []string
	}
	tests := []struct {
		name       string
		args       args
		wantResult string
	}{
		{
			name: "test-image",
			args: args{
				image:      "image",
				buildRoot:  "/tmp/buildroot",
				dockerFile: "dockerfile",
				buildArgs:  []string{"ARG1:value1", "ARG2:value2"},
			},
			wantResult: "docker build -f dockerfile --build-arg ARG1:value1 --build-arg ARG2:value2 --tag image /tmp/buildroot",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := BuilderCommand(tt.args.image, tt.args.buildRoot, tt.args.dockerFile, tt.args.buildArgs); gotResult != tt.wantResult {
				t.Errorf("BuilderCommand() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}
