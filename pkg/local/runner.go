package local

import "gitlab.com/pgmtc/le/pkg/common"

// Runner represents a runner
//go:generate mockgen -destination=./mocks/mock_runner.go -package=mocks gitlab.com/pgmtc/le/pkg/local Runner
type Runner interface {
	Create(ctx common.ModuleContext, cmp common.Component) error
	Remove(ctx common.ModuleContext, cmp common.Component) error
	Start(ctx common.ModuleContext, cmp common.Component) error
	Stop(ctx common.ModuleContext, cmp common.Component) error
	Pull(ctx common.ModuleContext, cmp common.Component) error
	Logs(ctx common.ModuleContext, cmp common.Component, follow bool) error
	Status(ctx common.ModuleContext, args ...string) error
	ExportCmd(ctx common.ModuleContext, cmp common.Component) error
}
