package local

import (
	"gitlab.com/pgmtc/le/pkg/common"
	"gitlab.com/pgmtc/le/pkg/docker"
)

// Module represents moduel
type Module struct{}

// GetActions returns list of actions
func (Module) GetActions() map[string]common.Action {
	runner := docker.Runner{}
	return map[string]common.Action{
		"default":    getRawAction(runner.Status),
		"status":     getRawAction(runner.Status),
		"create":     getComponentAction(runner.Create),
		"remove":     getComponentAction(runner.Remove),
		"start":      getComponentAction(runner.Start),
		"stop":       getComponentAction(runner.Stop),
		"pull":       getComponentAction(runner.Pull),
		"export-cmd": getComponentAction(runner.ExportCmd),
		"logs":       logsComponentAction(runner, false),
		"watch":      logsComponentAction(runner, true),
		"replace":    common.CompositeComponentAction(runner.Stop, runner.Remove, runner.Create, runner.Start),
		"raise":      common.CompositeComponentAction(runner.Create, runner.Start),
	}
}

func logsComponentAction(runner Runner, follow bool) common.Action {
	return &common.ComponentAction{
		Handler: func(ctx common.ModuleContext, cmp common.Component) error {
			return runner.Logs(ctx, cmp, follow)
		},
	}
}

func getComponentAction(handler common.ComponentActionHandler) common.Action {
	return &common.ComponentAction{
		Handler: handler,
	}
}

func getRawAction(handler common.RawActionHandler) common.Action {
	return &common.RawAction{
		Handler: handler,
	}
}
