package common

var components []Component

// Component represents a Component
type Component struct {
	Name          string   `yaml:"name,omitempty"`
	DockerID      string   `yaml:"dockerId,omitempty"`
	TestURL       string   `yaml:"testUrl,omitempty"`
	Image         string   `yaml:"image,omitempty"`
	ContainerPort int      `yaml:"containerPort,omitempty"`
	HostPort      int      `yaml:"hostPort,omitempty"`
	Ports         []string `yaml:"ports,omitempty"`
	Auth          string   `yaml:"auth,omitempty"`
	Env           []string `yaml:"env,omitempty"`        // environmental variables
	Links         []string `yaml:"links,omitempty"`      // --link in docker
	Mounts        []string `yaml:"mounts,omitempty"`     // mounts in docker
	ExtraHosts    []string `yaml:"extraHosts,omitempty"` // --add-host in docker
}

// ComponentNames returns list of available components
func ComponentNames(components []Component) []string {
	componentNames := []string{}
	for _, component := range components {
		componentNames = append(componentNames, component.Name)
	}
	return componentNames
}

// ComponentMap returns map of available components
func ComponentMap(components []Component) map[string]Component {
	elementMap := make(map[string]Component)
	for i := 0; i < len(components); i++ {
		elementMap[components[i].Name] = components[i]
	}
	return elementMap
}
