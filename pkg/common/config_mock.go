package common

import (
	"github.com/pkg/errors"
)

// MockConfig is used for testing
type MockConfig struct {
	failSaveRequired           bool
	failLoadRequired           bool
	saveConfigCalled           bool
	loadConfigCalled           bool
	saveProfileCalled          bool
	loadProfileCalled          bool
	getAvailableProfilesCalled bool
	currentProfileCalled       bool
	setProfileCalled           bool
	configCalled               bool

	currentProfileName string
	currentProfile     Profile
	config             Config
}

// SaveConfig saves the config
func (c *MockConfig) SaveConfig(overwrite bool) (fileName string, resultErr error) {
	c.saveConfigCalled = true
	if c.failSaveRequired {
		resultErr = errors.New("Deliberate testing error")
	}
	return
}

// LoadConfig loads the config
func (c *MockConfig) LoadConfig() (resultErr error) {
	c.loadConfigCalled = true
	if c.failLoadRequired {
		resultErr = errors.New("Deliberate testing error")
	}
	return
}

// SaveProfile saves the profile
func (c *MockConfig) SaveProfile(profileName string, profile Profile) (fileName string, resultErr error) {
	c.saveProfileCalled = true
	if c.failSaveRequired {
		resultErr = errors.New("Deliberate testing error")
	}
	return
}

// LoadProfile loads the profile
func (c *MockConfig) LoadProfile(profileName string) (profile Profile, resultErr error) {
	c.loadProfileCalled = true
	if c.failLoadRequired {
		resultErr = errors.New("Deliberate testing error")
		return
	}
	profile = c.currentProfile
	return
}

// GetAvailableProfiles lists available profiles
func (c *MockConfig) GetAvailableProfiles() (profiles []string) {
	c.getAvailableProfilesCalled = true
	return []string{c.currentProfileName}
}

// CurrentProfile returns current profile
func (c *MockConfig) CurrentProfile() Profile {
	c.currentProfileCalled = true
	return c.currentProfile
}

// SetProfile sets current profile
func (c *MockConfig) SetProfile(profileName string, profile Profile) {
	c.setProfileCalled = true
	c.currentProfileName = profileName
	c.currentProfile = profile
}

// Config returns config bit of configuration
func (c *MockConfig) Config() Config {
	c.configCalled = true
	return c.config
}

func (c *MockConfig) reset() *MockConfig {
	c.failLoadRequired = false
	c.failSaveRequired = false
	c.configCalled = false
	c.setProfileCalled = false
	c.currentProfileCalled = false
	c.getAvailableProfilesCalled = false
	c.loadProfileCalled = false
	c.saveProfileCalled = false
	c.loadConfigCalled = false
	c.saveConfigCalled = false
	return c
}

func (c *MockConfig) setLoadToFail() *MockConfig {
	c.failLoadRequired = true
	return c
}

func (c *MockConfig) setSaveToFail() *MockConfig {
	c.failSaveRequired = true
	return c
}

// LoadModuleConfig returns module config
func (c *MockConfig) LoadModuleConfig(moduleID string) interface{} {
	if moduleConfig, ok := c.config.ModuleConfig[moduleID]; ok {
		return moduleConfig
	}
	return nil
}

// SetModuleConfig sets module config
func (c *MockConfig) SetModuleConfig(moduleID string, moduleConfig interface{}) {
	c.config.ModuleConfig[moduleID] = moduleConfig
}

// ConfigLocation returns current location of the config
func (c *MockConfig) ConfigLocation() string {
	return ""
}

// CreateMockConfig is a constructor
func CreateMockConfig(components []Component) ConfigProvider {
	config := MockConfig{
		currentProfile: Profile{
			Components: components,
		},
	}
	return &config
}
