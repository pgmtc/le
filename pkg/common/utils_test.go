package common

import (
	"io/ioutil"
	"os"
	"os/user"
	"path"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

var (
	dockerTestingHappened = false
)

func TestArrContains(t *testing.T) {
	arr := []string{"element1", "element2", "element3"}
	var emptyArr []string

	if !ArrContains(arr, "element1") {
		t.Errorf("Expected true to be returned")
	}
	if ArrContains(arr, "nonExistent") {
		t.Errorf("Expected false to be returned")
	}
	if ArrContains(arr, "") {
		t.Errorf("Expected false to be returned")
	}
	if ArrContains(emptyArr, "element2") {
		t.Errorf("Expected false to be returned")
	}

}

func TestSkipDockerTesting_true(t *testing.T) {
	dockerTestingHappened = false
	origValue := os.Getenv("SKIP_DOCKER_TESTING")
	os.Setenv("SKIP_DOCKER_TESTING", "true")
	defer evalSkipDockerTesting(t, false, origValue)
	SkipDockerTesting(t)
	dockerTestingHappened = true // It should not get here
}

func TestParsePath(t *testing.T) {
	cwd, _ := os.Getwd()
	usr, _ := user.Current()
	winDirPrefix := ""
	if runtime.GOOS == "windows" {
		winDirPrefix = "C:"
	}
	type args struct {
		path string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "test-relative",
			args: args{path: filepath.FromSlash("relative/path.txt")},
			want: filepath.Join(cwd, "relative", "path.txt"),
		},
		{
			name: "test-absolute",
			args: args{path: filepath.FromSlash(winDirPrefix + "/absolute/relative/path.txt")},
			want: filepath.Join(winDirPrefix, "/absolute", "relative", "path.txt"),
		},
		{
			name: "test-home",
			args: args{path: "~/somedir/path.txt"},
			want: filepath.Join(usr.HomeDir, "somedir", "path.txt"),
		},
		{
			name: "test-cwd",
			args: args{path: "."},
			want: cwd,
		},
		//{
		//	name: "test-cwd-2",
		//	args: args{path: ""},
		//	want: cwd + filepath.FromSlash("/"),
		//},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParsePath(tt.args.path); got != tt.want {
				t.Errorf("ParsePath() = %v,\nwant %v", got, tt.want)
			}
		})
	}
}

func Test_YamlMarshall(t *testing.T) {
	type TestType struct {
		Name  string
		Value int
	}

	testData := TestType{
		Name:  "Test",
		Value: 10,
	}

	tmpDir, _ := ioutil.TempDir("", "le-test-mock")
	defer os.RemoveAll(tmpDir)

	tmpFile := tmpDir + "/YamlMarshall-out.yaml"
	junkFile := tmpDir + "/junk.yaml"
	ioutil.WriteFile(junkFile, []byte("junk file content\n"), 0644)

	// Test write success
	err := YamlMarshall(testData, tmpFile)
	if err != nil {
		t.Errorf("Unexpected error, got %s", err.Error())
	}

	// Test write failure (non existing directory)
	err = YamlMarshall(testData, "/non-existing"+tmpFile)
	if err == nil {
		t.Errorf("Expected error, got nothing")
	}

	outData := TestType{}
	err = YamlUnmarshall(tmpFile, &outData)
	if err != nil {
		t.Errorf("Unexpected error, got %s", err.Error())
	}

	if !reflect.DeepEqual(testData, outData) {
		t.Errorf("In and out does not match")
	}

	// Test Junk Content
	err = YamlUnmarshall(junkFile, &outData)
	if err == nil {
		t.Errorf("Expected error, got nothing")
	}

	// Test Non Existing file
	err = YamlUnmarshall(junkFile+"-non-existing", &outData)
	if err == nil {
		t.Errorf("Expected error, got nothing")
	}

}

func TestSkipDockerTesting_false(t *testing.T) {
	dockerTestingHappened = false
	origValue := os.Getenv("SKIP_DOCKER_TESTING")
	os.Unsetenv("SKIP_DOCKER_TESTING")
	defer evalSkipDockerTesting(t, true, origValue)
	SkipDockerTesting(t)
	dockerTestingHappened = true // It should get here
}

func evalSkipDockerTesting(t *testing.T, expectedValue bool, origValue string) {
	if origValue == "" {
		os.Unsetenv("SKIP_DOCKER_TESTING")
	} else {
		os.Setenv("SKIP_DOCKER_TESTING", origValue)
	}

	if expectedValue != dockerTestingHappened {
		t.Errorf("Unexpected dockerTestingHappened value, expected %t got %t", expectedValue, dockerTestingHappened)
	}
}

func Test_ParseCmdArgument(t *testing.T) {
	type args struct {
		argName string
		args    []string
	}
	tests := []struct {
		name          string
		args          args
		wantPresent   bool
		wantArgValues []string
	}{
		{
			name:          "test",
			args:          args{},
			wantPresent:   false,
			wantArgValues: nil,
		},
		{
			name: "test",
			args: args{
				argName: "arg3",
				args:    []string{"--arg1", "val1", "--arg2", "--arg3", "value3a", "--arg4", "--arg3", "--arg3", "value3b"},
			},
			wantPresent:   true,
			wantArgValues: []string{"value3a", "value3b"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotPresent, gotArgValues := ParseCmdArguments(tt.args.argName, tt.args.args...)
			if gotPresent != tt.wantPresent {
				t.Errorf("getArg() gotPresent = %v, want %v", gotPresent, tt.wantPresent)
			}
			if !reflect.DeepEqual(gotArgValues, tt.wantArgValues) {
				t.Errorf("getArg() gotArgValues = %v, want %v", gotArgValues, tt.wantArgValues)
			}
		})
	}
}

func TestParsePathRelativeTo(t *testing.T) {
	cwd, _ := os.Getwd()

	type args struct {
		path       string
		relativeTo string
	}
	tests := []struct {
		name       string
		args       args
		wantResult string
	}{
		{
			name: "test-not-provided",
			args: args{
				path:       ".subdir/file.txt",
				relativeTo: "",
			},
			wantResult: path.Join(cwd, ".subdir/file.txt"),
		},
		{
			name: "test-provided",
			args: args{
				path:       ".subdir/file.txt",
				relativeTo: "/tmp",
			},
			wantResult: path.Join("/tmp", ".subdir/file.txt"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := ParsePathRelativeTo(tt.args.path, tt.args.relativeTo); gotResult != tt.wantResult {
				t.Errorf("ParsePathRelativeTo() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestParseCmdArgument(t *testing.T) {
	type args struct {
		argName      string
		defaultValue string
		args         []string
	}
	tests := []struct {
		name       string
		args       args
		wantResult string
	}{
		{
			name: "test-present",
			args: args{
				argName:      "arg1",
				defaultValue: "defaultValue",
				args:         []string{"--arg0", "1", "--arg1", "value1", "--arg1", "value2", "--arg2", "value2"},
			},
			wantResult: "value1",
		},
		{
			name: "test-present",
			args: args{
				argName:      "argN",
				defaultValue: "defaultValue",
				args:         []string{"--arg0", "1", "--arg1", "value1", "--arg1", "value2", "--arg2", "value2"},
			},
			wantResult: "defaultValue",
		},
		{
			name: "test-present",
			args: args{
				argName:      "argN",
				defaultValue: "defaultValue",
				args:         []string{"--arg0", "1", "--arg1", "--arg2", "value2"},
			},
			wantResult: "defaultValue",
		},
		{
			name: "test-present",
			args: args{
				argName:      "argN",
				defaultValue: "defaultValue",
				args:         nil,
			},
			wantResult: "defaultValue",
		},
		{
			name: "test-present",
			args: args{
				argName:      "argN",
				defaultValue: "",
				args:         nil,
			},
			wantResult: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := ParseCmdArgument(tt.args.argName, tt.args.defaultValue, tt.args.args...); gotResult != tt.wantResult {
				t.Errorf("ParseCmdArgument() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}
