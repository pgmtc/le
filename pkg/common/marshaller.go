package common

//go:generate mockgen -destination=./mocks/mock_marshaller.go -package=mocks gitlab.com/pgmtc/le/pkg/common Marshaller

// Marshaller is an abstraction interface for marshalling
type Marshaller interface {
	Marshall(data interface{}, fileName string) (resultErr error)
	Unmarshall(fileName string, out interface{}) (resultErr error)
}

// YamlMarshaller represents YAML marshaller
type YamlMarshaller struct{}

// Marshall marshalls data
func (YamlMarshaller) Marshall(data interface{}, fileName string) (resultErr error) {
	return YamlMarshall(data, fileName)
}

// Unmarshall unmarshalls data
func (YamlMarshaller) Unmarshall(fileName string, out interface{}) (resultErr error) {
	return YamlUnmarshall(fileName, out)
}
