package common

import (
	"fmt"
)

// StringLogger is a logger implementation writing to a string
type StringLogger struct {
	DebugMessages []string
	InfoMessages  []string
	ErrorMessages []string
}

// Errorf prints an error
func (l *StringLogger) Errorf(format string, a ...interface{}) {
	l.ErrorMessages = append(l.ErrorMessages, fmt.Sprintf(format, a...))
}

// Debugf prints a debug message
func (l *StringLogger) Debugf(format string, a ...interface{}) {
	l.DebugMessages = append(l.DebugMessages, fmt.Sprintf(format, a...))
}

// Infof prints an info message
func (l *StringLogger) Infof(format string, a ...interface{}) {
	l.InfoMessages = append(l.InfoMessages, fmt.Sprintf(format, a...))
}

// Write prints a generic message
func (l *StringLogger) Write(p []byte) (n int, err error) {
	l.Infof("%s", p)
	return 0, nil
}
