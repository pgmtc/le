package common

import (
	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"os/user"
	"path/filepath"
	"strings"
	"testing"
)

// ArrContains returns true if item is in the array
func ArrContains(arr []string, value string) bool {
	for _, element := range arr {
		if element == value {
			return true
		}
	}
	return false
}

// ImageExists returns true if the image is in the list of images
func ImageExists(images []string, image string) bool {
	if ArrContains(images, image) {
		return true
	}
	imageNameBits := strings.Split(image, "/")
	bareImageName := imageNameBits[len(imageNameBits)-1]
	return ArrContains(images, bareImageName)
}

// SkipDockerTesting is a helper function
func SkipDockerTesting(t *testing.T) {
	if os.Getenv("SKIP_DOCKER_TESTING") != "" {
		t.Skip("Skipping docker testing")
	}
}

// ParsePath parses the path
func ParsePath(path string) (result string) {
	return ParsePathRelativeTo(path, "")
}

// ParsePathRelativeTo replaces relative path with absolute and replace ~ with user's home dir
// Relative paths are relative to the provided relativeTo parameter.
// If relativeTo is empty, cwd is used
func ParsePathRelativeTo(path string, relativeTo string) (result string) {
	usr, _ := user.Current()
	if relativeTo == "" {
		relativeTo, _ = os.Getwd()
	}
	if path == "." {
		path = relativeTo
	}
	result = strings.Replace(path, "~", usr.HomeDir, 1)

	if !filepath.IsAbs(result) {
		currentDir := relativeTo
		result = filepath.Join(currentDir, result)
	}

	//if runtime.GOOS == "windows" {
	//	match, _ := regexp.MatchString("[a-zA-Z]{1}:\\\\", result)
	//	if !match {
	//		currentDir, _ := os.Getwd()
	//		result = currentDir + "/" + result
	//	}
	//
	//} else {
	//	if !strings.HasPrefix(result, "/") {
	//		currentDir, _ := os.Getwd()
	//		result = currentDir + "/" + result
	//	}
	//}
	result = filepath.FromSlash(result)

	return
}

// YamlMarshall serializes data into yaml
func YamlMarshall(data interface{}, fileName string) (resultErr error) {
	bytes, _ := yaml.Marshal(data)
	if err := ioutil.WriteFile(fileName, bytes, 0644); err != nil {
		resultErr = errors.Errorf("error writing file: %s", err.Error())
		return
	}
	return
}

// YamlUnmarshall de-serializes data from yaml
func YamlUnmarshall(fileName string, out interface{}) (resultErr error) {
	bytes, err := ioutil.ReadFile(fileName)
	if err != nil {
		resultErr = errors.Errorf("error when opening file %s: %s", fileName, err.Error())
		return
	}

	if err := yaml.Unmarshal(bytes, out); err != nil {
		resultErr = errors.Errorf("error when unmarshalling file %s: %s", fileName, err.Error())
		return
	}
	return
}

// ParseCmdArguments sucks up value from command line arguments
func ParseCmdArguments(argName string, args ...string) (present bool, argValues []string) {
	fullArgName := "--" + argName
	for idx, arg := range args {
		if arg == fullArgName {
			present = true
			if len(args) <= idx+1 {
				return
			}
			nextArgValue := args[idx+1]
			if strings.HasPrefix(nextArgValue, "-") {
				continue
			}
			argValues = append(argValues, nextArgValue)
		}
	}
	return
}

// ParseCmdArgument returns an argument value, if not present, returns default value
func ParseCmdArgument(argName string, defaultValue string, args ...string) (result string) {
	present, parsed := ParseCmdArguments(argName, args...)
	if present && len(parsed) > 0 {
		result = parsed[0]
		return
	}
	result = defaultValue
	return
}
