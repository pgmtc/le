package common

// ConfigProvider is an interface for configs
//go:generate mockgen -destination=./mocks/mock_configprovider.go -package=mocks gitlab.com/pgmtc/le/pkg/common ConfigProvider
type ConfigProvider interface {
	SaveConfig(overwrite bool) (fileName string, resultErr error)
	LoadConfig() (resultErr error)
	SaveProfile(profileName string, profile Profile) (fileName string, resultErr error)
	LoadProfile(profileName string) (profile Profile, resultErr error)
	GetAvailableProfiles() (profiles []string)
	CurrentProfile() Profile
	SetProfile(profileName string, profile Profile)
	Config() Config
	ConfigLocation() string
	LoadModuleConfig(moduleID string) interface{}
	SetModuleConfig(moduleID string, moduleConfig interface{})
}

// Config is a config structure
type Config struct {
	Profile      string
	ModuleConfig map[string]interface{}
}

// DefaultProfile is a default profile struct
var DefaultProfile = Profile{
	Components: defaultComponents,
}

// Profile is a generic profile struct
type Profile struct {
	Components []Component
}

var defaultComponents = []Component{
	{
		Name:          "cmp1",
		Image:         "some-image-1:latest",
		ContainerPort: 8080,
		HostPort:      80,
		DockerID:      "container-1",
		TestURL:       "http://localhost:80/",
		Env:           []string{"ENV_1=value", "ENV_2=value2"},
	},
	{
		Name:          "cmp2",
		Image:         "some-image-2:latest",
		DockerID:      "container-2",
		ContainerPort: 8443,
		HostPort:      443,
		TestURL:       "http://localhost:443",
		Env:           []string{"ENV_1=value", "ENV_2=value2"},
		Auth:          "aws ecr get-login --no-include-email --region eu-west-1",
		Links: []string{
			"container-1:cmp1",
		},
		Mounts: []string{"/host-dir:/container-dir"},
	},
}
