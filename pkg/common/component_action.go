package common

import (
	"fmt"
	"github.com/pkg/errors"
)

// ComponentActionHandler is a function signature
type ComponentActionHandler func(ctx ModuleContext, cmp Component) error

// ComponentAction is an abstract action for the component
type ComponentAction struct {
	Handler ComponentActionHandler
}

// Run runs component action
func (a *ComponentAction) Run(ctx ModuleContext, args ...string) error {
	if len(args) == 0 {
		return errors.Errorf("Missing component Name. Available components = %s", ComponentNames(ctx.Config.CurrentProfile().Components))
	}

	// If all provided, do for all components
	if args[0] == "all" {
		for _, cmp := range ctx.Config.CurrentProfile().Components {
			err := a.Handler(ctx, cmp)
			if err != nil {
				fmt.Println(err.Error())
			}
		}
		return nil
	}

	for _, cmpName := range args {
		if component, ok := (ComponentMap(ctx.Config.CurrentProfile().Components))[cmpName]; ok {
			err := a.Handler(ctx, component)
			if err != nil {
				if len(args) > 1 {
					fmt.Println(err.Error())
				} else {
					return err
				}

			}
		} else {
			if len(args) > 1 { // Single use
				fmt.Printf("Component '%s' has not been found\n", cmpName)
			} else { // Multiple use
				return errors.Errorf("Component %s has not been found. Available components = %s", cmpName, ComponentNames(ctx.Config.CurrentProfile().Components))
			}
		}
	}
	return nil
}

// CompositeComponentAction returns one action for a series of actions
func CompositeComponentAction(actions ...ComponentActionHandler) Action {
	return &ComponentAction{
		Handler: func(ctx ModuleContext, cmp Component) error {
			for _, action := range actions {
				if err := action(ctx, cmp); err != nil {
					return err
				}
			}
			return nil
		},
	}
}

// CompositeAction returns one action for a series of actions
func CompositeAction(actions ...Action) Action {
	return &RawAction{
		Handler: func(ctx ModuleContext, args ...string) error {
			for _, action := range actions {
				if err := action.Run(ctx, args...); err != nil {
					return err
				}
			}
			return nil
		},
	}
}
