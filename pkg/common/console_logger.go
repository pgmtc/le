package common

import (
	"fmt"
	"github.com/fatih/color"
)

// ConsoleLogger is a struct representing console logger
type ConsoleLogger struct{}

// Errorf prints an error
func (ConsoleLogger) Errorf(format string, a ...interface{}) {
	_, _ = fmt.Fprintf(color.Output, color.HiRedString(format, a...))
}

// Debugf prints a debug message
func (ConsoleLogger) Debugf(format string, a ...interface{}) {
	_, _ = fmt.Fprintf(color.Output, color.HiBlackString(format, a...))
}

// Infof prints an info message
func (ConsoleLogger) Infof(format string, a ...interface{}) {
	_, _ = fmt.Fprintf(color.Output, color.WhiteString(format, a...))
}

// Write is a basic writter to the log
func (ConsoleLogger) Write(p []byte) (n int, err error) {
	return fmt.Printf("%s", p)
}
