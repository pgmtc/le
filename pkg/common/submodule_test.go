package common

import (
	"testing"
)

func TestSubAction(t *testing.T) {
	var subAction1Run bool
	var subAction1Args []string
	var subAction2Run bool
	subAction1 := RawAction{
		func(ctx ModuleContext, args ...string) error {
			subAction1Run = true
			subAction1Args = args
			return nil
		},
	}
	subAction2 := RawAction{
		func(ctx ModuleContext, args ...string) error {
			subAction2Run = true
			return nil
		},
	}

	subActions := map[string]Action{
		"sub1": &subAction1,
		"sub2": &subAction2,
	}

	subActionsToTest := SubAction(subActions)

	err := subActionsToTest.Run(ModuleContext{Log: ConsoleLogger{}, Config: CreateMockConfig([]Component{})}, "sub1")
	if err != nil {
		t.Errorf("unexpected error: %s", err.Error())
		return
	}
	if !subAction1Run {
		t.Errorf("subAction1Run = %t, expected %t", subAction1Run, true)
	}
	err = subActionsToTest.Run(ModuleContext{Log: ConsoleLogger{}, Config: CreateMockConfig([]Component{})}, "sub2")
	if !subAction2Run {
		t.Errorf("subAction1Run = %t, expected %t", subAction2Run, true)
	}
	err = subActionsToTest.Run(ModuleContext{Log: ConsoleLogger{}, Config: CreateMockConfig([]Component{})}, "sub1", "arg1", "arg2")
	if len(subAction1Args) != 2 || !ArrContains(subAction1Args, "arg1") || !ArrContains(subAction1Args, "arg2") {
		t.Errorf("unexpected in subAction1Args, expected arg1, arg2, got %v", subAction1Args)
	}
}
