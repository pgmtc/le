package common

import "context"

// Module represents an interface for modules
type Module interface {

	// GetActions returns map of actions for the module
	GetActions() map[string]Action
}

// Logger is an interface representing Logger
type Logger interface {
	Errorf(format string, a ...interface{})
	Debugf(format string, a ...interface{})
	Infof(format string, a ...interface{})
	Write(p []byte) (n int, err error)
}

// ModuleContext is a context
type ModuleContext struct {
	Log          Logger
	Config       ConfigProvider
	GlobalConfig ConfigProvider
	Module       Module
	ClientID     string // For remote running
	WorkingDir   string // for remote running
	Context      context.Context
}

// Action represents Action
type Action interface {
	Run(ctx ModuleContext, args ...string) error
}

// RawActionHandler represents RawActionHandler
type RawActionHandler func(ctx ModuleContext, args ...string) error

// RawAction represents raw action
type RawAction struct {
	Handler RawActionHandler
}

// Run runs the action
func (a *RawAction) Run(ctx ModuleContext, args ...string) error {
	return a.Handler(ctx, args...)
}
