package common

import (
	"fmt"
	"strings"
)

// SubAction allows to define set of sub actions
func SubAction(subActions map[string]Action) Action {
	return &RawAction{Handler: func(ctx ModuleContext, args ...string) error {
		// Build list of sub actions
		availableSubActions := make([]string, 0, len(subActions))
		for k := range subActions {
			availableSubActions = append(availableSubActions, k)
		}

		if len(args) == 0 {
			return fmt.Errorf("no sub action has been specified, available sub actions: %s", strings.Join(availableSubActions, ", "))
		}
		subID := args[0]
		if subAction, ok := subActions[subID]; ok {
			trimmedArgs := args[1:]
			return subAction.Run(ctx, trimmedArgs...)
		}
		return fmt.Errorf("sub action %s has not been found, available sub actions: %s", subID, strings.Join(availableSubActions, ", "))
	}}
}
