package agent

import (
	"encoding/base64"
	"gopkg.in/yaml.v2"
)

// ClientExportData represents agent connection data passed between server and client
type ClientExportData struct {
	AgentServer string
	Certificate string
}

// ParseClientExportData parses
func ParseClientExportData(data string) (result ClientExportData, resultErr error) {
	bytes, resultErr := base64.StdEncoding.DecodeString(data)
	if resultErr != nil {
		return
	}
	result = ClientExportData{}
	resultErr = yaml.Unmarshal(bytes, &result)
	return
}

// MarshallClientExportData marshals
func MarshallClientExportData(data ClientExportData) (result string, resultErr error) {
	bytes, resultErr := yaml.Marshal(data)
	if resultErr != nil {
		return
	}
	result = base64.StdEncoding.EncodeToString(bytes)
	return
}
