package agent

import (
	"encoding/base64"
	"fmt"
	"github.com/mholt/archiver"
	"gitlab.com/pgmtc/le/pkg/agent/agentclient"
	"gitlab.com/pgmtc/le/pkg/agent/config"
	"gitlab.com/pgmtc/le/pkg/common"
	"io/ioutil"
	"os"
	"path/filepath"
)

// Action for moving over local directory to the server workspace directory
func pushWDAction() common.Action {
	return &common.RawAction{
		Handler: func(ctx common.ModuleContext, args ...string) (resultErr error) {
			tmpDir, resultErr := ioutil.TempDir("", "")
			if resultErr != nil {
				return
			}
			archiveFile := "workdir.zip"
			archivePath := filepath.Join(tmpDir, archiveFile)
			defer os.Remove(archivePath)

			ctx.Log.Infof("creating archive ...")
			err := archiver.DefaultZip.Archive([]string{"."}, archivePath)
			if err != nil {
				ctx.Log.Infof("\n")
				return err
			}
			ctx.Log.Infof(" DONE\n")

			ctx.Log.Infof("files packed into file: %s\n", archivePath)

			// Cleanup remote directory
			if resultErr = remoteAgentExecuteAction("packer").Run(ctx, "cleanup"); resultErr != nil {
				return
			}

			// Pass on remote copy actions
			resultErr = remoteAgentAction(remoteCopyAction).Run(ctx, archivePath)
			if resultErr != nil {
				return
			}
			return remoteAgentExecuteAction("packer").Run(ctx, "unpack", archiveFile)
		},
	}
}

// This is the wrapper things like ping - each of these has to have counterpart in the server
func remoteAgentAction(handler func(client agentclient.AgentClient, args ...string) error) common.Action {
	return &common.RawAction{
		Handler: func(ctx common.ModuleContext, args ...string) error {
			client := agentclient.NewAgentClient(ctx)
			if err := client.Connect(); err != nil {
				return fmt.Errorf(err.Error())
			}
			defer client.Disconnect()
			return handler(client, args...)
		},
	}
}

// This is a generic wrapper for all actions which execute remote modules, such as cmd
func remoteAgentExecuteAction(remoteModule string) common.Action {
	return &common.RawAction{
		Handler: func(ctx common.ModuleContext, args ...string) error {
			client := agentclient.NewAgentClient(ctx)
			if err := client.Connect(); err != nil {
				return fmt.Errorf(err.Error())
			}
			defer client.Disconnect()
			args = append([]string{remoteModule}, args...)
			return client.Execute(args...)
		},
	}
}

// Runs ping receiver on the server
func remotePingAction(client agentclient.AgentClient, args ...string) error {
	return client.Ping()
}

// Runs copy receiver on the server
func remoteCopyAction(client agentclient.AgentClient, args ...string) error {
	if len(args) == 0 {
		return fmt.Errorf("missing file parameter")
	}
	filePath := args[0]
	return client.Upload(filePath)
}

// This is generic client connection method - connects to the client
func clientConnectAction() common.Action {
	return &common.RawAction{
		Handler: func(ctx common.ModuleContext, args ...string) error {
			if len(args) < 1 || args[0] == "" {
				return fmt.Errorf("missing import data")
			}
			importData := args[0]
			exportData, err := ParseClientExportData(importData)
			if err != nil {
				return fmt.Errorf("error when parsing import data: %s", err.Error())
			}
			// decode certificate
			certDecoded, err := base64.StdEncoding.DecodeString(exportData.Certificate)
			if err != nil {
				return fmt.Errorf("error when decoding server certificate: %s", err.Error())
			}
			// Write certfile to disk
			homeDir, _ := os.UserHomeDir()
			certFileName := filepath.Join(homeDir, ".le", fmt.Sprintf("%s.crt", exportData.AgentServer))

			if err = ioutil.WriteFile(certFileName, certDecoded, os.FileMode(0644)); err != nil {
				return fmt.Errorf("error when saving certificate to disk: %s", err.Error())
			}

			ctx.Log.Infof("Importing agent server connection\n"+
				" - agent server: %s (this is the connection string for remote connection)\n"+
				" - server cert : %s (this is the TLS certificate used for encryption)\n"+
				"", exportData.AgentServer, certFileName)
			ctx.Log.Infof(" if the agent server is incorrect, please go to %s/config.yaml file and edit it manually\n", ctx.Config.ConfigLocation())
			ctx.Log.Infof(" to test connection, run ")
			ctx.Log.Infof("le agent client ping\n")

			clientConfig := config.ClientConfig{
				AgentServer: exportData.AgentServer,
				CertFile:    certFileName,
			}

			ctx.Config.SetModuleConfig("agent-client", clientConfig)
			_, err = ctx.Config.SaveConfig(true)
			if err != nil {
				return err
			}
			return nil
		},
	}
}
