package agent

import (
	"encoding/base64"
	"fmt"
	"gitlab.com/pgmtc/le/pkg/agent/agentserver"
	"gitlab.com/pgmtc/le/pkg/agent/cert"
	"gitlab.com/pgmtc/le/pkg/agent/config"
	"gitlab.com/pgmtc/le/pkg/common"
	"io/ioutil"
	"os"
	"strings"
)

func serverInitAction() common.Action {
	return &common.RawAction{Handler: func(ctx common.ModuleContext, args ...string) error {
		_, extraHosts := common.ParseCmdArguments("host", args...)
		serverConfig := config.NewServerConfig(ctx.Config.ConfigLocation(), extraHosts)
		ctx.Config.SetModuleConfig("agent-server", serverConfig)
		_, err := ctx.Config.SaveConfig(true)
		if err != nil {
			return err
		}
		ctx.Log.Infof(" - config for agent-server module has been added to the main config file\n")

		// Generate certificates
		generateErr := serverGenerateCertificateAction().Run(ctx, args...)
		if generateErr != nil {
			return fmt.Errorf("error when generating certificates: %s", generateErr.Error())
		}

		// create workspaces directory, if not exists
		if _, err := os.Stat(serverConfig.WorkspacesLocation); os.IsNotExist(err) {
			if crErr := os.MkdirAll(serverConfig.WorkspacesLocation, os.ModePerm); crErr != nil {
				return fmt.Errorf("error when creating agent-workspace directory %s: %s", serverConfig.WorkspacesLocation, err.Error())
			}
			ctx.Log.Infof("- agent server workspace directory created: %s\n", serverConfig.WorkspacesLocation)
		}
		return nil
	}}
}

func serverStartAction() common.Action {
	return &common.RawAction{
		Handler: func(ctx common.ModuleContext, args ...string) error {
			return agentserver.NewAgentServer(ctx)
		},
	}
}

func serverGenerateCertificateAction() common.Action {
	return &common.RawAction{
		Handler: func(ctx common.ModuleContext, args ...string) error {
			serverConfig := config.ParseServerConfig(ctx)
			if len(serverConfig.Hosts) == 0 {
				return fmt.Errorf("missing certhosts property in the agent-server module config")
			}
			if serverConfig.CertFile == "" || serverConfig.KeyFile == "" {
				return fmt.Errorf("missing certfile / keyfile property in the agent-server module config")
			}

			// Check that files don't exist already
			force, _ := common.ParseCmdArguments("force", args...)
			certPath := common.ParsePath(serverConfig.CertFile)
			keyPath := common.ParsePath(serverConfig.KeyFile)

			certHosts := append(serverConfig.Hosts)

			if !force {
				inError := false
				if _, err := os.Stat(certPath); !os.IsNotExist(err) {
					ctx.Log.Errorf("file %s already exist, add --force to owerwrite\n", certPath)
					inError = true
				}
				if _, err := os.Stat(keyPath); !os.IsNotExist(err) {
					ctx.Log.Errorf("file %s already exist, add --force to owerwrite\n", keyPath)
					inError = true
				}
				if inError {
					return fmt.Errorf("failed to generate certificates")
				}
			}
			// Generate certificates
			ctx.Log.Infof("Generating certificate for the following hosts:\n - %s\n", strings.Join(certHosts, "\n - "))
			key, cert, err := cert.Generate(ctx.Log, certHosts)
			if err != nil {
				return err
			}
			// Write Cert File
			if writeErr := ioutil.WriteFile(certPath, []byte(cert), os.FileMode(0644)); writeErr != nil {
				return fmt.Errorf("error writing file: %s", certPath)
			}
			ctx.Log.Infof("- cert file saved : %s (copy this file over to your remote client)\n", certPath)
			// Write Key File
			if writeErr := ioutil.WriteFile(keyPath, []byte(key), os.FileMode(0644)); writeErr != nil {
				return fmt.Errorf("error writing file: %s", certPath)
			}
			ctx.Log.Infof("- key file saved  : %s (store this only on your server)\n", keyPath)
			return nil
		},
	}
}

func exportClientConfig() common.Action {
	return &common.RawAction{
		Handler: func(ctx common.ModuleContext, args ...string) error {
			// read certificate
			serverConfig := config.ParseServerConfig(ctx)
			certBytes, err := ioutil.ReadFile(serverConfig.CertFile)
			if err != nil {
				return fmt.Errorf("error when reading certificate file %s: %s", serverConfig.CertFile, err.Error())
			}
			if len(serverConfig.Hosts) == 0 {
				return fmt.Errorf("there are no hosts specified in the config file")
			}
			marshalled, err := MarshallClientExportData(ClientExportData{
				AgentServer: fmt.Sprintf("%s%s", serverConfig.Hosts[0], serverConfig.Listener),
				Certificate: base64.StdEncoding.EncodeToString(certBytes),
			})

			ctx.Log.Infof("---- run the following command on the client ----\n")
			ctx.Log.Infof("le agent login %s\n", marshalled)
			ctx.Log.Infof("---------------------------------------------\n")
			// grab client name from the arguments
			// create directory in the workspace
			// grab .crt file and base64 it
			return nil

		},
	}
}
