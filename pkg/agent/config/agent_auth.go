package config

import "context"

// AgentAuth holds the login/password
type AgentAuth struct {
	Workspace string
	ClientID  string
}

// GetRequestMetadata gets the current request metadata
func (a *AgentAuth) GetRequestMetadata(context.Context, ...string) (map[string]string, error) {
	return map[string]string{
		"clientid":  a.ClientID,
		"Workspace": a.Workspace,
	}, nil
}

// RequireTransportSecurity indicates whether the credentials requires transport security
func (a *AgentAuth) RequireTransportSecurity() bool {
	return true
}
