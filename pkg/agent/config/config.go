package config

import (
	"fmt"
	"gitlab.com/pgmtc/le/pkg/common"
	"os"
	"path/filepath"
)

// RemoteCommand represents command to run
type RemoteCommand string

// ServerConfig represents agent server config
type ServerConfig struct {
	WorkspacesLocation string
	Listener           string
	CertFile           string
	KeyFile            string
	Hosts              []string
	RemoteCommands     map[string]RemoteCommand
}

// ClientConfig represents client config
type ClientConfig struct {
	AgentServer string
	CertFile    string
}

// NewServerConfig is a constructor
func NewServerConfig(configLocation string, extraHosts []string) ServerConfig {
	hostName, _ := os.Hostname()
	return ServerConfig{
		//WorkspacesLocation: filepath.Join(configLocation, "agent-workspaces"),
		WorkspacesLocation: "clients",
		Listener:           ":8888",
		CertFile:           filepath.Join(configLocation, fmt.Sprintf("%s.crt", hostName)),
		KeyFile:            filepath.Join(configLocation, fmt.Sprintf("%s.key", hostName)),
		Hosts:              append(extraHosts, []string{"localhost", "127.0.0.1", hostName}...),
		RemoteCommands: map[string]RemoteCommand{
			"pwd":      "pwd",
			"hostname": "hostname",
			"ls":       "ls -ltr",
			"ping":     "ping -c3 @",
			"build":    "le builder build @",
		},
	}
}

// ParseServerConfig parses map or ServerConfig into ServerConfig
func ParseServerConfig(ctx common.ModuleContext) ServerConfig {
	configRaw := ctx.Config.LoadModuleConfig("agent-server")
	if configRaw == nil {
		return ServerConfig{}
	}
	if configAsServerConfig, ok := configRaw.(ServerConfig); ok {
		return configAsServerConfig
	}
	configMap := configRaw.(map[interface{}]interface{})
	var hostsRaw = configMap["hosts"].([]interface{})
	var hosts []string
	for _, host := range hostsRaw {
		hosts = append(hosts, fmt.Sprintf("%s", host))
	}
	// parse remote commands
	var remoteCommands = map[string]RemoteCommand{}
	for key, value := range configMap["remotecommands"].(map[interface{}]interface{}) {
		remoteCommands[key.(string)] = RemoteCommand(value.(string))
	}

	return ServerConfig{
		WorkspacesLocation: configMap["workspaceslocation"].(string),
		Listener:           configMap["listener"].(string),
		CertFile:           configMap["certfile"].(string),
		KeyFile:            configMap["keyfile"].(string),
		Hosts:              hosts,
		RemoteCommands:     remoteCommands,
	}
}

// ParseClientConfig parses ClientConfig or map into ClientConfig
func ParseClientConfig(ctx common.ModuleContext) ClientConfig {
	configRaw := ctx.GlobalConfig.LoadModuleConfig("agent-client")
	if configRaw == nil {
		return ClientConfig{}
	}
	if configAsClientConfig, ok := configRaw.(ClientConfig); ok {
		return configAsClientConfig
	}
	configMap := configRaw.(map[interface{}]interface{})
	return ClientConfig{
		AgentServer: configMap["agentserver"].(string),
		CertFile:    configMap["certfile"].(string),
	}
}
