package agent

import (
	"gitlab.com/pgmtc/le/pkg/common"
)

// Module represents a module
type Module struct{}

// GetActions return list of actions for a module
func (Module) GetActions() map[string]common.Action {
	return map[string]common.Action{
		"server": common.SubAction(map[string]common.Action{
			"init":          serverInitAction(),
			"start":         serverStartAction(),
			"client-config": exportClientConfig(),
		}),
		"login":  clientConnectAction(),
		"ping":   remoteAgentAction(remotePingAction),
		"remote": remoteAgentExecuteAction("remote run"),
		"push":   pushWDAction(),
	}
}
