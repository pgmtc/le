package agentclient

import (
	"fmt"
	"github.com/denisbrodbeck/machineid"
	"gitlab.com/pgmtc/le/pkg/agent/api"
	"gitlab.com/pgmtc/le/pkg/agent/config"
	"gitlab.com/pgmtc/le/pkg/common"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"os"
	"path/filepath"
)

// AgentClient represents Agent Interface
type AgentClient interface {
	Connect() error
	Disconnect() error
	Ping() error
	Upload(filePath string) error
	Execute(args ...string) error
}

type agentClient struct {
	conn        *grpc.ClientConn
	ctx         common.ModuleContext
	agentClient api.LeAgentClient
}

// NewAgentClient is a constructor
func NewAgentClient(ctx common.ModuleContext) AgentClient {
	return &agentClient{
		ctx: ctx,
	}
}

// Connects connects to remote server
func (c *agentClient) Connect() error {
	var err error
	clientConfig := config.ParseClientConfig(c.ctx)
	if clientConfig.CertFile == "" {
		return fmt.Errorf("missing certfile property in agent-client module clientConfig")
	}
	certFile := common.ParsePath(clientConfig.CertFile)
	creds, err := credentials.NewClientTLSFromFile(certFile, "")
	if err != nil {
		return fmt.Errorf("could not load TLS keys: %s", err)
	}
	wd, _ := os.Getwd()
	workspace := filepath.Base(wd)
	if workspace == "" {
		return fmt.Errorf("missing clientname property in agent-client module clientConfig")
	}
	machineID, err := machineid.ID()
	if err != nil {
		return fmt.Errorf("error when reading machine id: %s", err.Error())
	}

	perRPCAuth := config.AgentAuth{Workspace: workspace, ClientID: machineID}
	if c.conn, err = grpc.Dial(clientConfig.AgentServer, grpc.WithTransportCredentials(creds), grpc.WithPerRPCCredentials(&perRPCAuth)); err != nil {
		return fmt.Errorf("error when connecting to the remote agent: %s", err)
	}
	c.agentClient = api.NewLeAgentClient(c.conn)
	return nil
}

// Disconnect disconnects from remote server
func (c *agentClient) Disconnect() error {
	if c.conn != nil {
		return c.conn.Close()
	}
	return nil
}
