package agentclient

import (
	"context"
	"gitlab.com/pgmtc/le/pkg/agent/api"
	"io"
	"os"
	"path/filepath"
)

const chunkSize = 1024

// Upload sends a local file to the remote server
func (c *agentClient) Upload(filePath string) (resultErr error) {
	var file *os.File
	var fileInfo os.FileInfo
	var stream api.LeAgent_UploadClient
	if file, resultErr = os.Open(filePath); resultErr != nil {
		return
	}
	if fileInfo, resultErr = file.Stat(); resultErr != nil {
		return
	}
	if stream, resultErr = c.agentClient.Upload(context.Background()); resultErr != nil {
		return
	}

	sent := 0
	buf := make([]byte, chunkSize)
	for {
		var readBytes int
		if readBytes, resultErr = file.Read(buf); resultErr != nil {
			if resultErr == io.EOF {
				break
			}
			return
		}
		// Send chunk to the server
		if resultErr = stream.Send(&api.UploadChunk{Content: buf[:readBytes], FileName: filepath.Base(filePath)}); resultErr != nil {
			return
		}
		// Show percentage
		sent += chunkSize
		c.ctx.Log.Infof("\r%.2f pct", 100*float64(sent)/float64(fileInfo.Size()))
	}
	// Get final status
	status, err := stream.CloseAndRecv()
	if err != nil {
		c.ctx.Log.Infof("\n")
		return err
	}
	c.ctx.Log.Infof("\rSuccessfully sent %s. Remote location: %s\n", filepath.Base(filePath), status.Location)
	return nil
}
