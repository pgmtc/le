package agentclient

import (
	"context"
	"fmt"
	"gitlab.com/pgmtc/le/pkg/agent/api"
)

// Ping sends echo to the server
func (c *agentClient) Ping() error {
	if c.conn == nil {
		return fmt.Errorf("connection is nil, run Connect() first")
	}

	response, err := c.agentClient.Ping(context.Background(), &api.PingMessage{Message: "ping"})
	if err != nil {
		return fmt.Errorf("error when calling server: %s", err.Error())
	}
	c.ctx.Log.Infof("Response from server: %s\n", response.Message)
	return nil
}
