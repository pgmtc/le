package agentclient

import (
	"context"
	"fmt"
	"gitlab.com/pgmtc/le/pkg/agent/api"
	"io"
	"strings"
)

func (c *agentClient) Execute(args ...string) (resultErr error) {
	var stream api.LeAgent_ExecuteClient
	if stream, resultErr = c.agentClient.Execute(context.Background()); resultErr != nil {
		return
	}

	done := make(chan struct{})
	// First go routine sends commands to the server
	go func() {
		for {
			if err := stream.Send(&api.ExecuteRequest{Args: args}); err != nil {
				c.ctx.Log.Errorf("error when sending command to stream: %s\n", err.Error())
				return
			}
			return
		}
	}()
	// Second go routine receives responses from the server
	go func() {
		var resp *api.ExecuteResponse
		var err error
		for {
			if resp, err = stream.Recv(); err != nil {
				if err == io.EOF {
					done <- struct{}{}
					return
				}
				c.ctx.Log.Errorf("error when receiving message from stream: %s\n", err.Error())
				done <- struct{}{}
				return
			}

			// Print out the remote output
			if resp.Message == "" || resp.Message == "\n" || resp.Message == "\r" || resp.Message == "\n\r" {
				continue
			}
			msg := resp.Message
			if !strings.HasSuffix(msg, "\n") {
				msg = fmt.Sprintf("%s\n", msg)
			}
			c.ctx.Log.Infof("  %s", msg)
		}
	}()
	// Wait until server closes the communication
	<-done
	return nil
}
