package packer

import (
	"fmt"
	"github.com/mholt/archiver"
	"gitlab.com/pgmtc/le/pkg/common"
	"os"
	"path/filepath"
)

var serverLogger = common.ConsoleLogger{}

// Module represents a module
type Module struct{}

// GetActions return list of actions for a module
func (Module) GetActions() map[string]common.Action {
	return map[string]common.Action{
		"cleanup": cleanupAction(),
		"unpack":  unpackAction(),
	}
}

func cleanupAction() common.Action {
	return &common.RawAction{
		Handler: func(ctx common.ModuleContext, args ...string) (resultErr error) {
			if ctx.WorkingDir == "" {
				return fmt.Errorf("client workspace directory not set")
			}
			resultErr = removeDirContents(ctx.WorkingDir, ctx)
			return
		},
	}
}

func unpackAction() common.Action {
	return &common.RawAction{
		Handler: func(ctx common.ModuleContext, args ...string) (resultErr error) {
			if len(args) < 1 {
				return fmt.Errorf("missing arguments")
			}
			if ctx.WorkingDir == "" {
				return fmt.Errorf("client workspace directory not set")
			}

			fileName := args[0]

			archiveFile := filepath.Join(ctx.WorkingDir, fileName)
			destDir := filepath.Dir(archiveFile)
			resultErr = archiver.Unarchive(archiveFile, destDir)
			if resultErr != nil {
				return
			}
			serverLogger.Infof("file unpacked to %s\n", destDir)
			// remove archive
			resultErr = os.Remove(archiveFile)
			return
		},
	}
}

func removeDirContents(dir string, ctx common.ModuleContext) error {
	serverLogger.Infof("removing contents of %s\n", dir)

	d, err := os.Open(dir)
	if err != nil {
		return err
	}
	defer d.Close()
	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		err = os.RemoveAll(filepath.Join(dir, name))
		if err != nil {
			return err
		}
	}
	return nil
}
