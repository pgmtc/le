package cmd

import (
	"gitlab.com/pgmtc/le/pkg/common"
)

// Module represents moduel
type Module struct{}

// GetActions returns list of actions
func (Module) GetActions() map[string]common.Action {
	return map[string]common.Action{
		"run": runAction(),
	}
}
