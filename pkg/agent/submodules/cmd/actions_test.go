package cmd

import (
	config2 "gitlab.com/pgmtc/le/pkg/agent/config"
	"gitlab.com/pgmtc/le/pkg/common"
	"testing"
)

func Test_runCommand(t *testing.T) {
	//hostname, _ := os.Hostname()
	config := common.CreateMockConfig([]common.Component{})
	log := common.ConsoleLogger{}
	ctx := common.ModuleContext{
		Log:    log,
		Config: config,
	}

	type args struct {
		ctx     common.ModuleContext
		command config2.RemoteCommand
		args    []string
	}
	tests := []struct {
		name       string
		args       args
		wantResult string
		wantErr    bool
	}{
		{
			name: "test-ls",
			args: args{
				ctx:     ctx,
				command: "hostname",
				args:    nil,
			},
			wantResult: "", // no result, it is printed
			wantErr:    false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResult, err := runCommand(tt.args.ctx, tt.args.command, tt.args.args...)
			if (err != nil) != tt.wantErr {
				t.Errorf("runCommand() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotResult != tt.wantResult {
				t.Errorf("runCommand() gotResult = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}
