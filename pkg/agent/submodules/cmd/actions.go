package cmd

import (
	"bufio"
	"fmt"
	"gitlab.com/pgmtc/le/pkg/agent/config"
	"gitlab.com/pgmtc/le/pkg/common"
	"os/exec"
	"strings"
)

var serverLogger = common.ConsoleLogger{}

func runAction() common.Action {
	return &common.RawAction{
		Handler: func(ctx common.ModuleContext, args ...string) error {
			if len(args) < 1 {
				return fmt.Errorf("missing command to run")
			}
			serverConfig := config.ParseServerConfig(ctx)
			var cmd config.RemoteCommand
			var ok bool
			if cmd, ok = serverConfig.RemoteCommands[args[0]]; !ok {
				return fmt.Errorf("command %s does not exist", args[0])

			}
			output, err := runCommand(ctx, cmd, args[1:]...)
			//ctx.Log.Debugf("command output:  %s", strings.ReplaceAll(output, "\n", "\n  "))
			ctx.Log.Debugf(output)
			ctx.Log.Infof("\r")
			return err
		},
	}
}

func runCommand(ctx common.ModuleContext, command config.RemoteCommand, args ...string) (result string, resultErr error) {
	cmdParts := strings.Split(string(command), " ")
	if len(cmdParts) > 1 && cmdParts[len(cmdParts)-1] == "@" {
		// allowed to pass custom parameters
		cmdParts = append(cmdParts[:len(cmdParts)-1], args...)
	}

	serverLogger.Infof("client %s running in %s: %s %s\n", ctx.ClientID, ctx.WorkingDir, cmdParts[0], strings.Join(cmdParts[1:], " "))
	cmd := exec.Command(fmt.Sprintf("%s", cmdParts[0]), cmdParts[1:]...)
	cmd.Dir = ctx.WorkingDir
	stdout, _ := cmd.StdoutPipe()
	stderr, _ := cmd.StderrPipe()
	if resultErr = cmd.Start(); resultErr != nil {
		return
	}
	scanner := bufio.NewScanner(stdout)
	scanner.Split(bufio.ScanLines)
	scannerErr := bufio.NewScanner(stderr)
	scannerErr.Split(bufio.ScanLines)

	// This is run either when the command finishes, or parent context calls for cancel
	done := make(chan error, 1)

	go func() {
		for scanner.Scan() {
			m := scanner.Text()
			ctx.Log.Infof("%s\n", m)
		}
	}()

	go func() {
		for scannerErr.Scan() {
			m := scannerErr.Text()
			ctx.Log.Errorf("%s\n", m)
		}
	}()

	// Wait for command to finish
	go func() {
		done <- cmd.Wait()
	}()
	// Wait for parent context to cancel
	if ctx.Context != nil {
		go func() {
			<-ctx.Context.Done()
			done <- cmd.Process.Kill()
		}()
	}

	if err := <-done; err != nil {
		serverLogger.Errorf(err.Error())
	}
	//bytes, resultErr := cmd.CombinedOutput()

	result = "" //string(bytes)
	return
}
