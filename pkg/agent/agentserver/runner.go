package agentserver

import (
	"context"
	"fmt"
	"gitlab.com/pgmtc/le/pkg/agent/config"
	"gitlab.com/pgmtc/le/pkg/agent/submodules/cmd"
	"gitlab.com/pgmtc/le/pkg/agent/submodules/packer"
	"gitlab.com/pgmtc/le/pkg/common"
	"reflect"
	"strings"
)

var workspaceScopeModules = map[string]common.Module{
	"packer": packer.Module{}, // used for push / unpacking
	"remote": cmd.Module{},
}

var serverScopeModules = map[string]common.Module{
	"cmd": cmd.Module{},
}

func (s *AgentServer) runAgentAction(context context.Context, log common.Logger, agentAuth config.AgentAuth, clientWorkspaceDir string, args ...string) error {
	if len(args) < 2 {
		return fmt.Errorf("module and/or action has not been provided")
	}
	argSplit := strings.Split(args[0], " ")
	argSplit = append(argSplit, args[1:]...)

	moduleName := argSplit[0]
	actionName := argSplit[1]
	var module common.Module
	isServerScope := false
	var ok bool
	if module, ok = workspaceScopeModules[moduleName]; !ok {
		if module, ok = serverScopeModules[moduleName]; !ok {
			return fmt.Errorf("module %s does not exist", moduleName)
		}
		isServerScope = true
	}

	//moduleArgs := args[1:]
	if actionName == "" {
		actionName = "default"
	}
	actions := module.GetActions()
	if _, ok := actions[actionName]; !ok {
		availableActions := reflect.ValueOf(actions).MapKeys()
		return fmt.Errorf("missing action '%s'. Available actions: %s", actionName, availableActions)
	}
	action := actions[actionName]
	workingDir := clientWorkspaceDir
	if isServerScope {
		workingDir = "."
	}
	return s.runWorkspaceAction(context, action, log, agentAuth, workingDir, argSplit[2:]...)
}

func (s *AgentServer) runWorkspaceAction(context context.Context, action common.Action, log common.Logger, agentAuth config.AgentAuth, clientWorkspaceDir string, args ...string) error {
	if err := action.Run(common.ModuleContext{
		Context:    context,
		Log:        log,
		Config:     s.ctx.Config,
		ClientID:   agentAuth.ClientID,
		WorkingDir: clientWorkspaceDir}, args...); err != nil {
		msg := fmt.Sprintf("action error: %s\n", strings.Replace(err.Error(), "\n", "", -1))
		return fmt.Errorf(msg)
	}
	return nil
}
