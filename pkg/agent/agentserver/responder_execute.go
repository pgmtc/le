package agentserver

import (
	"fmt"
	"gitlab.com/pgmtc/le/pkg/agent/api"
	"io"
)

// Execute is a responder for execute command
func (s *AgentServer) Execute(stream api.LeAgent_ExecuteServer) error {
	clientAuth := s.getClientAuth(stream.Context())
	clientWorkspacePath, err := s.getClientWorkspacePath(stream.Context())
	if err != nil {
		return err
	}
	ctx := stream.Context()
	for {
		select {
		case <-ctx.Done():
			s.ctx.Log.Infof("ctx is done")
			return ctx.Err()
		default:
		}

		req, err := stream.Recv()
		if err == io.EOF {
			/// return will close stream from server side
			s.ctx.Log.Infof("exiting")
			return nil
		}
		if err != nil {
			return fmt.Errorf("error when receiving stream: %s", err.Error())
		}

		// Create responder which will pass responses back
		rm := remoteLogger{
			stream: stream,
			log:    s.ctx.Log,
		}
		// Run action
		err = s.runAgentAction(ctx, rm, clientAuth, clientWorkspacePath, req.Args...)
		if err != nil {
			rm.Errorf("error when running agent action:%s\n", err.Error())
		}
		return nil
	}
}
