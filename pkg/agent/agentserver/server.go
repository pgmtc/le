package agentserver

import (
	"context"
	"fmt"
	"github.com/apex/log"
	"github.com/apex/log/handlers/text"
	"gitlab.com/pgmtc/le/pkg/agent/api"
	"gitlab.com/pgmtc/le/pkg/agent/config"
	"gitlab.com/pgmtc/le/pkg/common"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
	"net"
	"os"
	"path/filepath"
	"strings"
)

func init() {
	log.SetHandler(text.Default)
	log.SetLevel(log.DebugLevel)
}

// AgentServer represents the gRPC server
type AgentServer struct {
	ctx    common.ModuleContext
	config config.ServerConfig
}

// NewAgentServer is a constructor
func NewAgentServer(ctx common.ModuleContext) error {
	// load module serverConfig
	serverConfig := config.ParseServerConfig(ctx)
	// Check for cert file, keyfile
	if serverConfig.CertFile == "" || serverConfig.KeyFile == "" || serverConfig.Listener == "" {
		return fmt.Errorf("missing certfile / keyfile / listener properties in agent-server module serverConfig")
	}
	certFile := common.ParsePath(serverConfig.CertFile)
	keyFile := common.ParsePath(serverConfig.KeyFile)
	creds, err := credentials.NewServerTLSFromFile(certFile, keyFile)
	if err != nil {
		return fmt.Errorf("error when loading TLS keys: %s", err.Error())
	}

	// create a listener
	lis, err := net.Listen("tcp", serverConfig.Listener)
	if err != nil {
		return fmt.Errorf("failed to listen: %s", err.Error())
	}
	// create a server instance
	s := AgentServer{
		ctx:    ctx,
		config: serverConfig,
	}
	grpcServer := grpc.NewServer(grpc.Creds(creds))
	// attach the Ping service to the server
	api.RegisterLeAgentServer(grpcServer, &s)
	// start the server
	ctx.Log.Infof("Agent server starting on %s\n", serverConfig.Listener)
	ctx.Log.Debugf("- cert file: %s\n", serverConfig.CertFile)
	ctx.Log.Debugf("- key file: %s\n", serverConfig.CertFile)

	if err := grpcServer.Serve(lis); err != nil {
		return fmt.Errorf("failed to server: %s", err.Error())
	}
	return nil
}

// This is for decoding auth info provided by the client
func (s *AgentServer) getClientAuth(ctx context.Context) (auth config.AgentAuth) {
	clientID := ""
	workspace := ""
	if md, ok := metadata.FromIncomingContext(ctx); ok {
		clientID = strings.Join(md["clientid"], "")
		workspace = strings.Join(md["workspace"], "")
	}
	return config.AgentAuth{
		Workspace: workspace,
		ClientID:  clientID,
	}
}

// This is for determining what is the current working directory for each connection
func (s *AgentServer) getClientWorkspacePath(ctx context.Context) (clientWorkspacePath string, resultErr error) {
	auth := s.getClientAuth(ctx)
	if auth.ClientID == "" {
		resultErr = fmt.Errorf("unable to identify client")
		return
	}
	if auth.Workspace == "" {
		resultErr = fmt.Errorf("unable to identify workspace")
	}

	leAgentDir := common.ParsePath(s.config.WorkspacesLocation)
	if _, err := os.Stat(leAgentDir); os.IsNotExist(err) {
		resultErr = fmt.Errorf("leagentdirectory %s does not exist", leAgentDir)
		return
	}
	// Get client directory
	clientDir := filepath.Join(leAgentDir, auth.ClientID, auth.Workspace)
	if _, err := os.Stat(clientDir); os.IsNotExist(err) {
		s.ctx.Log.Infof("client directory %s does not exist, creating ...\n", clientDir)
		if createErr := os.MkdirAll(clientDir, os.ModePerm); createErr != nil {
			resultErr = fmt.Errorf("error when creating client directory %s: %s", clientDir, createErr.Error())
			s.ctx.Log.Errorf("%s\n", resultErr)
			return
		}
		return
	}
	clientWorkspacePath, _ = filepath.Abs(clientDir)
	return
}
