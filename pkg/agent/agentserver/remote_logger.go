package agentserver

import (
	"fmt"
	"gitlab.com/pgmtc/le/pkg/agent/api"
	"gitlab.com/pgmtc/le/pkg/common"
	"strings"
)

var writeBuf []byte

type remoteLogger struct {
	stream api.LeAgent_ExecuteServer
	log    common.Logger
}

func (r remoteLogger) Errorf(format string, a ...interface{}) {
	r.sendRemote(fmt.Sprintf(format, a...))
}

func (r remoteLogger) Debugf(format string, a ...interface{}) {
	r.sendRemote(fmt.Sprintf(format, a...))
}

func (r remoteLogger) Infof(format string, a ...interface{}) {
	r.sendRemote(fmt.Sprintf(format, a...))
}

func (r remoteLogger) Write(p []byte) (n int, err error) {
	writeBuf = append(writeBuf, p...)
	if strings.Contains(string(writeBuf), "\n") {
		msg := strings.Replace(string(writeBuf), "\n", "", -1)
		r.sendRemote(msg)
		writeBuf = []byte{}
	}
	return len(p), nil
}

func (r remoteLogger) sendRemote(message string) {
	message = strings.TrimSuffix(message, "\n")
	err := r.stream.Send(&api.ExecuteResponse{Message: message})
	if err != nil && !strings.Contains(err.Error(), "transport is closing") {
		r.log.Errorf("error when sending response %s: %s\n", message, err.Error())
	}
}
