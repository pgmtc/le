package agentserver

import (
	"context"
	"gitlab.com/pgmtc/le/pkg/agent/api"
)

// Ping generates response to a Ping request
func (s *AgentServer) Ping(ctx context.Context, in *api.PingMessage) (*api.PingMessage, error) {
	agentAuth := s.getClientAuth(ctx)
	s.ctx.Log.Infof("Received message from %s for workspace %s: %s, responding with pong\n", agentAuth.ClientID, agentAuth.Workspace, in.Message)
	return &api.PingMessage{Message: "pong"}, nil
}
