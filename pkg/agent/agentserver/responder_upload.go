package agentserver

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/pgmtc/le/pkg/agent/api"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
)

// Upload is a responder for upload file
func (s *AgentServer) Upload(stream api.LeAgent_UploadServer) error {
	targetDir, err := s.getClientWorkspacePath(stream.Context())
	if err != nil {
		return err
	}
	tempFile, err := ioutil.TempFile(os.TempDir(), "")
	if err != nil {
		fmt.Println("Cannot create temporary file", err)
	}

	received := 0
	fileName := ""
	for {
		chunk, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				// done with the transfer
				break
			}
			err = errors.Wrapf(err, "failed unexpectedly while reading chunks from stream")
			return err
		}
		fileName = chunk.FileName
		received += len(chunk.Content)

		s.ctx.Log.Infof("Receiving file, received %d bytes\r", received)
		_, err = tempFile.Write(chunk.Content)
		if err != nil {
			return errors.Wrapf(err, "error when writing file")
		}
	}
	err = tempFile.Close()
	if err != nil {
		return errors.Wrap(err, "error when closing the file")
	}
	// Rename file to original file name
	finalFile := filepath.Join(targetDir, fileName)
	//if err = os.Rename(tempFile.Name(), finalFile); err != nil {
	//	return errors.Wrapf(err, "error when renaming file %s to %s", tempFile.Name(), finalFile)
	//}
	input, err := ioutil.ReadFile(tempFile.Name())
	if err != nil {
		return errors.Wrapf(err, "error when reading file")
	}
	err = ioutil.WriteFile(finalFile, input, os.FileMode(0644))
	if err != nil {
		return errors.Wrapf(err, "error when writing file")
	}
	// remove old file
	err = os.Remove(tempFile.Name())
	if err != nil {
		return errors.Wrapf(err, "error when removing file %s", tempFile.Name())
	}

	return stream.SendAndClose(&api.UploadStatus{
		Location: finalFile,
		Code:     api.UploadStatusCode_Ok,
	})
}
