package builder

import (
	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"
	"gitlab.com/pgmtc/le/pkg/common"
	"gitlab.com/pgmtc/le/pkg/common/mocks"
	"os"
	"testing"
)

func cleanup() {
	if _, err := os.Stat(builderDir); !os.IsNotExist(err) {
		os.RemoveAll(builderDir)
	}
}

func setUpInit(t *testing.T) (mockCtrl *gomock.Controller, mockFsHandler *mocks.MockFSHandler, mockMarshaller *mocks.MockMarshaller, action common.Action) {
	mockCtrl = gomock.NewController(t)
	mockFsHandler = mocks.NewMockFSHandler(mockCtrl)
	mockMarshaller = mocks.NewMockMarshaller(mockCtrl)
	action = initAction(mockFsHandler, mockMarshaller)
	return
}

func Test_initAction_success(t *testing.T) {
	mockCtrl, mockFsHandler, mockMarshaller, action := setUpInit(t)
	defer mockCtrl.Finish()

	// Success scenario
	mockFsHandler.EXPECT().Stat(gomock.Any()).Return(nil, os.ErrNotExist)
	mockFsHandler.EXPECT().MkdirAll(gomock.Any(), gomock.Any()).Return(nil)
	mockFsHandler.EXPECT().Stat(gomock.Any()).Return(nil, os.ErrNotExist)
	mockMarshaller.EXPECT().Marshall(gomock.Any(), gomock.Any()).Return(nil)
	mockFsHandler.EXPECT().Create(gomock.Any()).Return(nil, nil)
	// Test image
	err := action.Run(common.ModuleContext{
		Config: common.CreateMockConfig([]common.Component{}),
		Log:    common.ConsoleLogger{},
	}, "image")
	if err != nil {
		t.Errorf("Unexpected error: %s", err.Error())
	}

	// Test file
	// Success scenario
	mockFsHandler.EXPECT().Stat(gomock.Any()).Return(nil, os.ErrNotExist)
	mockFsHandler.EXPECT().MkdirAll(gomock.Any(), gomock.Any()).Return(nil)
	mockFsHandler.EXPECT().Stat(gomock.Any()).Return(nil, os.ErrNotExist)
	mockMarshaller.EXPECT().Marshall(gomock.Any(), gomock.Any()).Return(nil)
	err = action.Run(common.ModuleContext{
		Config: common.CreateMockConfig([]common.Component{}),
		Log:    common.ConsoleLogger{},
	}, "file")
	if err != nil {
		t.Errorf("Unexpected error: %s", err.Error())
	}
}

func Test_initAction_dirExists(t *testing.T) {
	mockCtrl, mockFsHandler, _, action := setUpInit(t)
	defer mockCtrl.Finish()

	// Success scenario
	mockFsHandler.EXPECT().Stat(gomock.Any()).Return(nil, nil)         // dir exists
	mockFsHandler.EXPECT().Stat(gomock.Any()).Return(nil, os.ErrExist) //

	err := action.Run(common.ModuleContext{
		Config: common.CreateMockConfig([]common.Component{}),
		Log:    common.ConsoleLogger{},
	}, "image")
	if err == nil {
		t.Errorf("Expected error, got nothing")
	}
}

func Test_initAction_createFailure(t *testing.T) {
	mockCtrl, mockFsHandler, _, action := setUpInit(t)
	defer mockCtrl.Finish()

	// Success scenario
	mockFsHandler.EXPECT().Stat(gomock.Any()).Return(nil, os.ErrNotExist)
	mockFsHandler.EXPECT().MkdirAll(gomock.Any(), gomock.Any()).Return(errors.New("mock error"))
	err := action.Run(common.ModuleContext{
		Config: common.CreateMockConfig([]common.Component{}),
		Log:    common.ConsoleLogger{},
	}, "image")
	if err == nil {
		t.Errorf("Expected error, got nothing")
	}
}

func Test_initAction_marshallFailure(t *testing.T) {
	mockCtrl, mockFsHandler, mockMarshaller, action := setUpInit(t)
	defer mockCtrl.Finish()

	// Success scenario
	mockFsHandler.EXPECT().Stat(gomock.Any()).Return(nil, os.ErrNotExist)
	mockFsHandler.EXPECT().MkdirAll(gomock.Any(), gomock.Any()).Return(nil)
	mockFsHandler.EXPECT().Stat(gomock.Any()).Return(nil, os.ErrNotExist)
	mockMarshaller.EXPECT().Marshall(gomock.Any(), gomock.Any()).Return(errors.New("mock error"))
	err := action.Run(common.ModuleContext{
		Config: common.CreateMockConfig([]common.Component{}),
		Log:    common.ConsoleLogger{},
	}, "image")
	if err == nil {
		t.Errorf("Expected error, got nothing")
	}
}

func Test_initAction_writeFailure(t *testing.T) {
	mockCtrl, mockFsHandler, mockMarshaller, action := setUpInit(t)
	defer mockCtrl.Finish()

	// Success scenario
	mockFsHandler.EXPECT().Stat(gomock.Any()).Return(nil, os.ErrNotExist)
	mockFsHandler.EXPECT().MkdirAll(gomock.Any(), gomock.Any()).Return(nil)
	mockFsHandler.EXPECT().Stat(gomock.Any()).Return(nil, os.ErrNotExist)
	mockMarshaller.EXPECT().Marshall(gomock.Any(), gomock.Any()).Return(nil)
	mockFsHandler.EXPECT().Create(gomock.Any()).Return(nil, errors.New("mock error"))
	err := action.Run(common.ModuleContext{
		Config: common.CreateMockConfig([]common.Component{}),
		Log:    common.ConsoleLogger{},
	}, "image")
	if err == nil {
		t.Errorf("Expected error, got nothing")
	}
}
