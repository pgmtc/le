package builder

import (
	"gitlab.com/pgmtc/le/pkg/common"
)

//go:generate mockgen -destination=./mocks/mock_builder.go -package=mocks gitlab.com/pgmtc/le/pkg/builder Builder

// Builder represents interface for builder
type Builder interface {
	BuildImage(ctx common.ModuleContext, image string, buildRoot string, dockerFile string, buildArgs []string, noCache bool) error
	BuildFile(ctx common.ModuleContext, file string, buildRoot string, builderImage string, builderCmd string, noCache bool) error
	ExportCmd(ctx common.ModuleContext, image string, buildRoot string, dockerFile string, buildArgs []string, file string, builderImage string, builderCmd string) error
}
