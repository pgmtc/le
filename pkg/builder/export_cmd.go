package builder

import (
	"fmt"
	"gitlab.com/pgmtc/le/pkg/common"
	"os"
	"path/filepath"
)

func getExportCmdAction(builder Builder) common.Action {
	return &common.RawAction{
		Handler: func(ctx common.ModuleContext, args ...string) error {
			specDir := common.ParseCmdArgument("specdir", "", args...)
			workingDir, _ := os.Getwd()
			if ctx.WorkingDir != "" {
				specDir = filepath.Join(ctx.WorkingDir, builderDir)
				workingDir = ctx.WorkingDir
			}
			image, buildRoot, dockerFile, buildArgs, file, builderImage, builderCmd, err := parseBuildProperties(workingDir, specDir)
			if err != nil {
				return fmt.Errorf("error when parsing build properties: %s", err.Error())
			}
			return builder.ExportCmd(ctx, image, buildRoot, dockerFile, buildArgs, file, builderImage, builderCmd)
		},
	}
}
