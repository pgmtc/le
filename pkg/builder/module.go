package builder

import (
	"gitlab.com/pgmtc/le/pkg/common"
	"gitlab.com/pgmtc/le/pkg/docker"
)

// Module represents a module
type Module struct{}

// GetActions return list of actions for a module
func (Module) GetActions() map[string]common.Action {
	builder := docker.Builder{}
	return map[string]common.Action{
		"build":      getBuildAction(builder),
		"init":       initAction(common.OsFileSystemHandler{}, common.YamlMarshaller{}),
		"export-cmd": getExportCmdAction(builder),
	}
}
