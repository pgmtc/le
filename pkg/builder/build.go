package builder

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/pgmtc/le/pkg/common"
	"os"
	"path"
	"path/filepath"
)

func getBuildAction(builder Builder) common.Action {
	return &common.RawAction{
		Handler: func(ctx common.ModuleContext, args ...string) error {
			noCache, _ := common.ParseCmdArguments("nocache", args...)
			specDir := common.ParseCmdArgument("specdir", "", args...)
			tag := common.ParseCmdArgument("tag", "latest", args...)

			// for remote runs
			workingDir, _ := os.Getwd()
			if ctx.WorkingDir != "" {
				specDir = filepath.Join(ctx.WorkingDir, builderDir)
				workingDir = ctx.WorkingDir
			}
			ctx.Log.Infof("Running build. SpecDir = %s, workingDir = %s\n", specDir, workingDir)
			image, buildRoot, dockerFile, buildArgs, file, builderImage, builderCmd, err := parseBuildProperties(workingDir, specDir)
			// Override this for remote builds
			switch true {
			case err != nil:
				return err
			case image != "" && file != "":
				return fmt.Errorf("config must have either image OR file property, not both")
			case image != "":
				return builder.BuildImage(ctx, image+":"+tag, buildRoot, dockerFile, buildArgs, noCache)
			case file != "":
				ctx.Log.Debugf("want to build a file: %s, %s, %s\n", file, builderImage, builderCmd)
				return builder.BuildFile(ctx, file, buildRoot, builderImage, builderCmd, noCache)
			}
			return fmt.Errorf("config must have either image or file set")
		},
	}
}

func parseBuildProperties(workingDir string, specDir string) (image string, buildRoot string, dockerFile string, buildArgs []string, file string, builderImage string, builderCmd string, resultErr error) {
	// Try to read builder config
	if specDir == "" {
		specDir = builderDir
	}
	specDirPath := common.ParsePath(specDir)
	if _, err := os.Stat(specDirPath); os.IsNotExist(err) {
		resultErr = errors.Errorf("Unable to determine build configuration: %s", err.Error())
		return
	}

	bcnf := buildConfig{}
	bcnfPath := path.Join(specDir, configFileName)
	err := common.YamlUnmarshall(bcnfPath, &bcnf)
	if err != nil {
		resultErr = errors.Errorf("Unable to parse config file %s: %s", bcnfPath, err.Error())
		return
	}
	image = bcnf.Image
	buildRoot = common.ParsePathRelativeTo(bcnf.BuildRoot, workingDir)
	dockerFile = common.ParsePathRelativeTo(bcnf.Dockerfile, workingDir)
	//fmt.Println(specDir)
	//fmt.Println(dockerFile)
	//fmt.Println(buildRoot)
	buildArgs = bcnf.BuildArgs
	file = bcnf.File
	builderImage = bcnf.BuilderImage
	builderCmd = bcnf.BuilderCmd
	return
}
