package builder

// mockgen -destination=mocks/mock_builder.go -package=mocks gitlab.com/pgmtc/le/pkg/builder Builder

import (
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pgmtc/le/pkg/builder/mocks"
	"io/ioutil"
	"os"
	"testing"

	"github.com/golang/mock/gomock"
	"gitlab.com/pgmtc/le/pkg/common"
)

func setUp() (tmpDir string, mockContext common.ModuleContext) {
	tmpDir, _ = ioutil.TempDir("", "le-test-mock")
	os.MkdirAll(tmpDir+"/buildtest", os.ModePerm)
	ioutil.WriteFile(tmpDir+"/buildtest/builder.yaml", []byte(""+
		"image: test-image\n"+
		"buildroot: "+tmpDir+"/buildtest/\n"+
		"dockerfile: "+tmpDir+"/buildtest/Dockerfile\n"+
		"builderimage: test-builder-image\n"+
		"buildargs: \n"+
		"  - arg1:\"val1\"\n"+
		"  - arg2:\"val2\"\n"+
		"buildercmd: test-builder-cmd"), 0644)
	ioutil.WriteFile(tmpDir+"/buildtest/Dockerfile", []byte("FROM scratch\nADD . ."), 0644)

	os.MkdirAll(tmpDir+"/buildtest-invalid", os.ModePerm)
	ioutil.WriteFile(tmpDir+"/buildtest-invalid/config.yaml", []byte("some:unparsable:rubbish"), 0644)

	config := common.CreateMockConfig([]common.Component{})
	mockContext = common.ModuleContext{
		Config: config,
		Log:    &common.StringLogger{},
	}
	return
}

func Test_parseBuildProperties(t *testing.T) {
	tmpDir, _ := setUp()
	buildDir := tmpDir + "/buildtest"
	image, buildDir, dockerFile, buildArgs, file, builderImage, builderCmd, err := parseBuildProperties(tmpDir, buildDir)
	expectedImage := "test-image"
	expectedBuildDir := tmpDir + "/buildtest/"
	expectedDockerfile := tmpDir + "/buildtest/Dockerfile"
	expectedBuildArgs := []string{"arg1:\"val1\"", "arg2:\"val2\""}
	expectedFile := ""
	expectedBuilderImage := "test-builder-image"
	expectedBuilderCmd := "test-builder-cmd"
	assert.NoError(t, err)
	assert.Equal(t, expectedImage, image)
	assert.Equal(t, expectedBuildDir, buildDir)
	assert.Equal(t, expectedDockerfile, dockerFile)
	assert.Equal(t, expectedBuildArgs, buildArgs)
	assert.Equal(t, expectedFile, file)
	assert.Equal(t, expectedBuilderImage, builderImage)
	assert.Equal(t, expectedBuilderCmd, builderCmd)

	// Test error - non existing build dir
	buildDir = tmpDir + "/non-existing"
	image, buildDir, dockerFile, buildArgs, file, builderImage, builderCmd, err = parseBuildProperties(tmpDir, buildDir)
	if err == nil {
		t.Errorf("Expected error, got nothing")
	}

	// Test error - non non-parsable config file
	buildDir = tmpDir + "/buildtest-invalid"
	image, buildDir, dockerFile, buildArgs, file, builderImage, builderCmd, err = parseBuildProperties(tmpDir, buildDir)
	if err == nil {
		t.Errorf("Expected error, got nothing")
	}
}

func Test_buildAction(t *testing.T) {
	tmpDir, mockContext := setUp()
	buildContext := tmpDir + "/buildtest"

	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()
	mockBuilder := mocks.NewMockBuilder(mockCtrl)
	buildAction := getBuildAction(mockBuilder)

	// Try missing specdir parameter
	if err := buildAction.Run(mockContext, "--specdir"); err == nil {
		t.Errorf("Expected error, got nothing")
	}
	// Test non-existing specdir parameter
	if err := buildAction.Run(mockContext, "--nocache"); err == nil {
		t.Errorf("Expected error, got nothing")
	}

	// Test success
	expectedImage := "test-image:latest"
	expectedBuildRoot := buildContext + "/"
	expectedDockerFile := buildContext + "/Dockerfile"
	expectedCtx := mockContext

	mockBuilder.EXPECT().BuildImage(expectedCtx, expectedImage, expectedBuildRoot, expectedDockerFile, []string{"arg1:\"val1\"", "arg2:\"val2\""}, true).Return(nil).Times(1)
	if err := buildAction.Run(mockContext, "--specdir", buildContext, "--nocache"); err != nil {
		t.Errorf("Unexpected error: %s", err.Error())
	}

	// Test no cache set to false
	mockBuilder.EXPECT().BuildImage(expectedCtx, expectedImage, expectedBuildRoot, expectedDockerFile, []string{"arg1:\"val1\"", "arg2:\"val2\""}, false).Return(nil).Times(1)
	if err := buildAction.Run(mockContext, "--specdir", buildContext); err != nil {
		t.Errorf("Unexpected error: %s", err.Error())
	}

	// Test no cache set to false
	mockBuilder.EXPECT().BuildImage(expectedCtx, expectedImage, expectedBuildRoot, expectedDockerFile, []string{"arg1:\"val1\"", "arg2:\"val2\""}, false).Return(nil).Times(1)
	if err := buildAction.Run(mockContext, "--specdir", buildContext); err != nil {
		t.Errorf("Unexpected error: %s", err.Error())
	}

	// Test that error is passed through
	mockBuilder.EXPECT().BuildImage(expectedCtx, expectedImage, expectedBuildRoot, expectedDockerFile, []string{"arg1:\"val1\"", "arg2:\"val2\""}, false).Return(errors.Errorf("artificial error")).Times(1)
	if err := buildAction.Run(mockContext, "--specdir", buildContext); err == nil {
		t.Errorf("Expected error, got nothing")
	}
}
