package builder

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/pgmtc/le/pkg/common"
	"os"
	"path"
	"path/filepath"
	"strings"
)

var builderDir = ".le"
var configFileName = "builder.yaml"

const defaultDockerFile = ".le/Dockerfile"
const defaultBuildRoot = ""
const defaultBuilderFile = "plugin.so"
const defaultBuilderImage = "golang:1.12.8"
const defaultBuilderCmd = "go build -buildmode=plugin -o plugin.so"

func init() {
	// test if there is .le directory present
	if _, err := os.Stat(builderDir); !os.IsNotExist(err) {
		return
	}
	// try .builder - backward compatibility
	if _, err := os.Stat(".builder"); os.IsNotExist(err) {
		return
	}
	// .builder exists, check for .builder/config.yaml - backward compatibility
	if _, err := os.Stat(filepath.Join(".builder", "config.yaml")); !os.IsNotExist(err) {
		// .le/builder.yaml exists, use that for the config
		builderDir = ".builder"
		configFileName = "config.yaml"
	}
}

type buildConfig struct {
	Image        string   `yaml:"image,omitempty"`
	BuildRoot    string   `yaml:"buildroot,omitempty"`
	Dockerfile   string   `yaml:"dockerfile,omitempty"`
	BuildArgs    []string `yaml:"buildargs,omitempty"`
	File         string   `yaml:"file,omitempty"`
	BuilderImage string   `yaml:"builderimage,omitempty"`
	BuilderCmd   string   `yaml:"buildercmd,omitempty"`
}

func initAction(fsHandler common.FSHandler, marshaller common.Marshaller) common.Action {
	return &common.RawAction{
		Handler: func(ctx common.ModuleContext, args ...string) error {
			bcnf := buildConfig{}
			createDockerFile := false
			switch true {
			case len(args) == 0:
				return fmt.Errorf("please provide type of the builder. Available types: file, image")
			case strings.ToLower(args[0]) == "image":
				bcnf = buildConfig{
					Image:      "my-image",
					BuildRoot:  defaultBuildRoot,
					Dockerfile: defaultDockerFile,
					BuildArgs:  []string{"build_arg_1:example_value"},
				}
				createDockerFile = true
			case strings.ToLower(args[0]) == "file":
				bcnf = buildConfig{
					File:         defaultBuilderFile,
					BuildRoot:    defaultBuildRoot,
					BuilderImage: defaultBuilderImage,
					BuilderCmd:   defaultBuilderCmd,
				}
			default:
				return fmt.Errorf("unknown builder type. Available types: file, image")
			}

			configDirPath := common.ParsePath(builderDir)
			if _, err := fsHandler.Stat(configDirPath); os.IsNotExist(err) {
				if err := fsHandler.MkdirAll(configDirPath, os.ModePerm); err != nil {
					return errors.Errorf("Error when creating config directory: %s", err.Error())
				}
			}
			configPath := path.Join(configDirPath, configFileName)
			if _, err := fsHandler.Stat(configPath); !os.IsNotExist(err) {
				return errors.Errorf("File %s already exists, remove it first", configPath)
			}

			if err := marshaller.Marshall(bcnf, configPath); err != nil {
				return errors.Errorf("Error when writing build config: %s", err.Error())
			}

			if createDockerFile {
				// Create empty dockerfile
				dfPath := path.Join(configDirPath, strings.Replace(defaultDockerFile, builderDir, "", 1))
				if _, err := fsHandler.Create(dfPath); err != nil {
					return errors.Errorf("Error when writing empty Dockerfile: %s", err.Error())
				}
			}

			return nil
		},
	}
}
