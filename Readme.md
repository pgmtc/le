# local environment (le)
[![pipeline status](https://gitlab.com/pgmtc/le/badges/master/pipeline.svg)](https://gitlab.com/pgmtc/le/commits/master)
[![coverage report](https://gitlab.com/pgmtc/le/badges/master/coverage.svg)](https://gitlab.com/pgmtc/le/commits/master)

## Prerequisites
1. Running docker daemon

## Installation
### macOS
* Add pgmtc homebrew repository
```
brew tap pgmtc/repo
brew update
```
* Install le tool
```
brew install pgmtc/repo/le
```
* Init config
```
le config init
```

### Manual
1. Download appropriate package from [releases page](https://gitlab.com/pgmtc/le/releases)
2. Unzip it, there should be an executable inside
3. Put it somewhere to path, potentially chmod a+x it
4. Run it from the terminal / command line

## Usage 
Syntax:

`le [module] [action] parameters`

In cases related to containers (vast majority), syntax is as follows:

- `le [module] [action] component` : runs action for component
- `le [module] [action] component1 component2 ... componentN` : runs for component1 .. componentN
- `le [module] [action] all` : runs for all available components


## Modules
### local
Local module is responsible for running local environments
It has the following actions

`le local status`: prints status of the local environment

`le local pull [component]`: used for components with remote docker images

`le local create [component]`: create a docker container for the component

`le local remove [component]`: removes docker container of the component

`le local start [component]`: starts docker container for the component

`le local stop [component]`: stops the docker container for the component

`le local logs [component]`: shows logs of the related docker container

`le local watch [component]`: shows logs on the 'follow' basis

`le local export-cmd [component]`: prints out docker command equivalent for running the container

### builder
`le builder build`: builds a docker image 

`le builder export-cmd`: prints out docker command equivalent for building the container

Build definition is stored in .builder directory inside the project.
It can be override by providing --specdir argument.
To create empty build definition directory, run 
`le builder init`



### config
Config is a centralized storage used by other modules.

`le config init`: Run after the installation. Creates ~/.le, config file and default profile

`le config status'`: Prints out information about the current profile. Adding -v makes it more verbose

`le config create [profile] [source-profile]`: Creates a new profile. By passing source-profile parameter (not mandatory), it uses it as a base for copy

`le config switch [profile]`: Switches current profile to another one

## agent
Very much work in progress. Agent can be used for 
- running remote commands on agent server
- pushing local source code to the remote server and building remotely
- various other things (CI/CD, automation)

Commnication between agent server and agent client is via GRPC and it uses TLS. Each client has a unique generated ID to make sure different clients don't clash among themselves.
### Quick Start
#### initialize server (machine 1)
- create an empty directory somewhere and cd into it
- run `le config init` (this initializes le config files)
- run `le agent server init [--host hostname_accessible_over_network]` (this initializes agent server config files and generates certificates)
- run `le agent server client-config` (this prints out the le command which should be run on the client)
- run `le agent server start` (starts the agent server)

If not set up globally, server configuration is stored im $(pwd)/.le directory. All server settings are in $(pwd)/.le/config.yaml file in moduleconfig/agent-server section.
Once the server is started, `clients` directory is created in current working directory. Client's workspaces are then stored in this directory.

#### initialize client (machine 2)
- create an empty directory somewhere and cd into it
- if not done previously, run `le config init --global` (this initializes le global config, where client connection is stored)
- run `le agent login [TOKEN]` (this imports the 'token' generated on the server side)
- run `le agent ping` (this pings the server, if everything working, it should write 'pong')

### running remote commands
Agent client can run remote commands on agent server. These commands are limited to what is in the agent server configuration file. There are two ways to publish the command for clients to be used
- static command: these commands don't consume any extra parameters from the clients
- dynamic command: these commands consume parameters send from the client. This can pose a security risk so consider, which commands should be exposed
#### exposing commands on the server
All commands are defined in agent server configuration section in .le/config.yaml (or ~/.le/config,yaml for global configurations)
- To expose static command, simply define it as "command": "what to run". For example "ping-google": "ping -c 3 www.google.com"
- To expose dynamic command, define it the same way, just add "@" at the end. For example "ping": "ping -c 3 @" 
- There are a few commands exposed as an example, check .le/config.yaml file (pwd, ls, hostname, ping, build)
  ```
    remotecommands:
      build: le builder build @
      hostname: hostname
      ls: ls -ltr
      ping: ping -c3 @
      pwd: pwd
      # just a cheeky idea of routing requests to yet another server upstream
      another-server-remote: le agent remote @ 
  ```
- to run the command from the client machine, run
  ```
  le agent remote ping www.google.com
  le agent remote hostname
  le agent remote build --nocache
  ```